#cd $(dirname $0)
echo "Pairing..."
expect pair_bluetooth_device.expect > expect_script.log
chmod 777 expect_script.log
sleep 2

echo "Trusting and connecting.."
device_mac_address=$(grep -Pom 1 "(?<=Device ).*?(?= )" < expect_script.log)
echo mac address is $device_mac_address
if [[ ! -z $device_mac_address ]] ; then
            expect pair_trust_connect.expect $device_mac_address
else
            echo "No device connected"
fi
rm expect_script.log
