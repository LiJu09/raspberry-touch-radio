import cgi
import datetime
import json
import re
import socket
import subprocess
import threading
import time
import tkinter as tk
from collections import defaultdict
from functools import wraps
from http.server import BaseHTTPRequestHandler, HTTPServer

import alsaaudio
import dbus
import dbus.mainloop.glib
import feedparser
import INA226
import qrcode
import requests
import yaml
import yt_dlp
from PIL import ImageTk, Image
from dateutil import parser
from gi.repository import GLib
from guizero import App, PushButton, Text, Box, Window, Picture, Slider, ListBox
from pyradios import RadioBrowser
from resizeimage import resizeimage
from websock import WebSocketServer
from youtube_search import YoutubeSearch

VERSION = "0.0.0"

MPC_PATH = "/usr/bin/mpc"

MAIN = "main"
CLOCK = "clock"
CALENDAR = "calendar"
PLAYLIST_VIEW = "playlist_view"
RADIO_BROWSER = "radio_browser"
MENU = "menu"
WEATHER = "weather"
FORECAST = "forecast"
RSS_MENU = "rss_menu"
RSS_VIEW = "rss_view"
QR_CODE = "qr_code"
EQUALIZER_LOW = "equalizer_low"
EQUALIZER_HIGH = "equalizer_high"
SETTINGS_LOCK = "settings_lock"
RESET_CONFIG = "reset_config"
SETTINGS = "settings"
AUDIO_MENU = "audio_menu"
USB_VIDEO = "usb_video"
IMAGE_VIEW = "image_view"
BLUETOOTH_CONNECT = "bluetooth_connect"
POWER_MENU = "power_menu"
POWER_MENU_CONFIRM = "power_menu_confirm"
RESTART = "restart"
SHUTDOWN = "shutdown"
NOTICE = "notice"

RADIO = "radio"
USB = "usb"
BLUETOOTH = "bluetooth"
YOUTUBE = "youtube"

EQUAL_CONTROLS = [
    '00. 31 Hz',
    '01. 63 Hz',
    '02. 125 Hz',
    '03. 250 Hz',
    '04. 500 Hz',
    '05. 1 kHz',
    '06. 2 kHz',
    '07. 4 kHz',
    '08. 8 kHz',
    '09. 16 kHz'
]
MIXER_DEVICE = {'device': 'equal'}

YDL_OPTIONS = {'format': 'bestaudio'}
# YDL_OPTIONS = {'format': 'bestaudio', 'noplaylist': 'True'}
# ydl = youtube_dl.YoutubeDL({'outtmpl': '%(id)s%(ext)s'})
# ydl.add_default_info_extractors()
RADIO_BROWSER_FIRST = ["Krajina", "Žáner"]
RADIO_BROWSER_SORT = ["Spustenia", "Hlasy", "Názov"]
RBS_FIRST = "first"
RBS_FIRST_CHOOSING = "first_choose"
RBS_SORT = "sort"
RBS_SORT_CHOOSING = "sort_choose"
RBS_RESULTS = "rbs_results"


class Throttle:
    """
    https://gist.github.com/ChrisTM/5834503
    Decorator that prevents a function from being called more than once every
    time period.
    To create a function that cannot be called more than once a minute:
        @throttle(minutes=1)
        def my_fun():
            pass
    """

    def __init__(self, milliseconds=0, seconds=0, minutes=0, hours=0):
        self.throttle_period = datetime.timedelta(milliseconds=milliseconds, seconds=seconds, minutes=minutes,
                                                  hours=hours)
        self.time_of_last_call = datetime.datetime.min

    def __call__(self, fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            now = datetime.datetime.now()
            time_since_last_call = now - self.time_of_last_call

            if time_since_last_call > self.throttle_period:
                self.time_of_last_call = now
                return fn(*args, **kwargs)

        return wrapper


class Server(BaseHTTPRequestHandler):

    def do_AUTHHEAD(self):
        self.send_response(401)
        self.send_header('WWW-Authenticate', 'Basic realm=\"Access to config\"')
        self.send_header('Content-type', 'text/plain')
        self.end_headers()

    def do_HEAD(self, content_type='plain'):
        self.send_response(200)
        self.send_header('Content-type', content_type + '; charset=UTF-8')
        self.end_headers()

    def do_GET(self):
        if self.path == '/' or self.path == '/index.html':
            self.do_HEAD('text/html')
            self.wfile.write(
                server_files.index_html
                .replace("VERSION_HERE".encode("utf-8"), f"v{VERSION}".encode("utf-8"))
                .replace("CONFIG_HERE".encode("utf-8"), config.read_config().encode("utf-8"))
                # .replace("RSS_LINKS_HERE".encode("utf-8"), rss_manager.get_rss_links_html().encode("utf-8"))
                # .replace("PLAYLIST_HERE".encode("utf-8"), player_manager.get_playlist_json().encode("utf-8"))
            )
        elif self.path == '/main.css':
            self.do_HEAD('text/css')
            self.wfile.write(server_files.main_css)
        elif self.path == '/main.js':
            self.do_HEAD('text/javascript')
            self.wfile.write(server_files.main_js)
        elif self.path == '/raspberrytouch.webmanifest':
            self.do_HEAD('application/manifest+json')
            self.wfile.write(server_files.webmanifest)

        elif self.path == '/restart':
            self.do_HEAD('text/plain')
            power_restart()
        elif self.path == '/restartapp':
            self.do_HEAD('text/plain')
            power_restart()
        elif self.path == '/reloadwebserver':
            self.do_HEAD('text/plain')
            server_files.load_files()
            ws_manager.send_to_all("reload_page")
        elif self.path == '/windowpath':
            self.do_HEAD('text/plain')
            self.wfile.write(str(window_manager.window_path).encode("utf-8"))
        elif self.path == '/audiosource':
            self.do_HEAD('text/plain')
            self.wfile.write(str(player_manager.audio_source).encode("utf-8"))
        elif self.path == '/wsclients':
            self.do_HEAD('text/plain')
            self.wfile.write(str(ws_manager.ws_clients).encode("utf-8"))

        elif self.path == '/icon.png':
            self.do_HEAD('image/png')
            with open('icon/radio.png', 'rb') as file_handle:
                self.wfile.write(file_handle.read())

    def do_POST(self):
        if self.path == '/?saveconfig':
            self.do_HEAD('plain')
            form = cgi.FieldStorage(fp=self.rfile, headers=self.headers, environ={"REQUEST_METHOD": "POST"})
            conf = form.getvalue("config")
            conf = conf.replace("\\n", "\n")
            saved = config.write_config_safe(conf)
            if saved:
                self.wfile.write("OK".encode("utf-8"))
            else:
                self.wfile.write("ERROR".encode("utf-8"))


class ServerFiles:
    def __init__(self):
        self.index_html = b""
        self.main_css = b""
        self.main_js = b""
        self.webmanifest = b""
        # self.load_files()

    def load_files(self):
        with open('server/index.html') as file:
            self.index_html = file.read().encode("utf-8")
        with open('server/main.css') as file:
            self.main_css = file.read().encode("utf-8")
        with open('server/main.js') as file:
            self.main_js = file.read().encode("utf-8")
        with open('server/raspberrytouch.webmanifest') as file:
            self.webmanifest = file.read().encode("utf-8")


class Config:
    def __init__(self, file_path):
        self.config = defaultdict()
        self.can_read_write = False
        self.file_path = file_path
        self.error_message = ""
        self.load_config()

    def load_config(self):
        try:
            with open(self.file_path) as file:
                config_file = yaml.load(file, Loader=yaml.SafeLoader)
            self.config = config_file
            self.config['config_key'] = str(self.config.get('config_key', ''))
            self.config['volume_step'] = str(self.config.get('volume_step', '2'))
            if self.config['owm_api_key'] is None:
                self.config['owm_api_key'] = ""
                self.error_message += "- OWM API key\n"
                temperature_btn.hide()
                open_weather_btn.disable()
            if self.config['owm_city'] is None:
                self.config['owm_city'] = ""
                self.error_message += "- OWM city\n"
                temperature_btn.hide()
                open_weather_btn.disable()
            if self.config['owm_lang'] is None:
                self.config['owm_lang'] = "en"
                self.error_message += "- OWM lang\n"
            if self.config['equalizer'] is None:
                self.config['equalizer'] = "78:76:74:72:70:66:66:66:66:66"
            if self.config['rss_links'] is None:
                self.config['rss_links'] = []

            if self.error_message != "":
                self.error_message = self.error_message + "is missing.\nCheck config and restart"

        except FileNotFoundError:
            self.config['owm_api_key'] = ""
            self.config['owm_city'] = ""
            self.config['owm_lang'] = ""
            self.equalizer = "78:76:74:72:70:66:66:66:66:66"
            self.pin = ""
            self.reset_config()
            self.error_message = "Config file not found\nCreated new with default values"
            temperature_btn.hide()
            open_weather_btn.disable()

    def write_config(self, config_data):
        with open(self.file_path, "w") as file:
            file.write(config_data)

    def write_config_safe(self, config_data):
        if self.can_read_write:
            config_file = yaml.load(config_data, Loader=yaml.SafeLoader)
            with open(self.file_path) as file:
                config_file_on_disk = yaml.load(file, Loader=yaml.SafeLoader)

            for k, v in config_file.items():
                if v == "~~~~":
                    config_file[k] = config_file_on_disk[k]

            for key, value in config_file_on_disk.items():
                if key not in config_file:
                    config_file[key] = value

            with open(self.file_path, "w") as file:
                yaml.dump(config_file, file)

            return True
        else:
            return False

            # self.toggle_allow_read_write()

    def read_config(self):
        if self.can_read_write:
            with open(self.file_path) as file:
                return file.read()
        else:
            return ""

    def read_config_safe(self):
        if self.can_read_write:
            with open(self.file_path) as file:
                config_file = yaml.load(file, Loader=yaml.SafeLoader)
            config_data = ""
            for k, v in config_file.items():
                if k in ["config_key", "rss_links", "equalizer"]:
                    continue
                if k in ["config_key", "owm_api_key"]:
                    v = "~~~~"
                config_data += f"{k}: {v}\n"
            return config_data
        else:
            return ""

    def read_rss_links(self):
        with open(self.file_path) as file:
            config_file = yaml.load(file, Loader=yaml.SafeLoader)
        return config_file.get('rss_links')

    def add_rss_link(self, rss_link):
        with open(self.file_path) as file:
            config_file = yaml.load(file, Loader=yaml.SafeLoader)
        rss_links = config_file.get('rss_links', [])
        if rss_links is not None:
            if rss_link not in rss_links:
                rss_links.append(rss_link)
        else:
            rss_links = [rss_link]
        config_file['rss_links'] = rss_links
        with open(config.file_path, "w") as file:
            yaml.dump(config_file, file)
        self.load_config()
        rss_manager.reload_links()

    def remove_rss_link(self, name):
        with open(self.file_path) as file:
            config_file = yaml.load(file, Loader=yaml.SafeLoader)
        rss_links = config_file.get('rss_links')

        if name in rss_manager.rss_names:
            rss_link_index = rss_manager.rss_names.index(name)
            rss_links.pop(rss_link_index)
        else:
            rss_links.remove(name)

        config_file['rss_links'] = rss_links
        with open(config.file_path, "w") as file:
            yaml.dump(config_file, file)
        self.load_config()
        rss_manager.reload_links()

    def move_rss_link(self, index, direction):
        with open(self.file_path) as file:
            config_file = yaml.load(file, Loader=yaml.SafeLoader)
        index = int(index)
        rss_links = config_file.get('rss_links')
        if direction == "UP":
            if index == 0:
                rss_links.insert(len(rss_links) - 1, rss_links.pop(index))
            elif index > 0:
                rss_links[index], rss_links[index - 1] = rss_links[index - 1], rss_links[index]
        elif direction == "DOWN":
            if index == len(rss_links) - 1:
                rss_links.insert(0, rss_links.pop(index))
            elif index < len(rss_links) - 1:
                rss_links[index], rss_links[index + 1] = rss_links[index + 1], rss_links[index]
        config_file['rss_links'] = rss_links
        with open(config.file_path, "w") as file:
            yaml.dump(config_file, file)
        self.load_config()
        rss_manager.reload_links()

    def move_rss_link_up(self):
        index = rss_links_listbox.items.index(rss_links_listbox.value)
        threading.Thread(target=self.move_rss_link, name="config.move_rss_link UP", args=[index, "UP"], daemon=True).start()

    def move_rss_link_down(self):
        index = rss_links_listbox.items.index(rss_links_listbox.value)
        threading.Thread(target=self.move_rss_link, name="config.move_rss_link DOWN", args=[index, "DOWN"], daemon=True).start()

    def reset_config(self):
        config_data = """config_key: 1234
equalizer: 78:76:74:72:70:66:66:66:66:66
owm_api_key:
owm_city:
owm_lang:
"""
        self.write_config(config_data)
        window_manager.close_window(RESET_CONFIG, "Config Reset to default values")

    def toggle_allow_read_write(self):
        self.can_read_write = not self.can_read_write
        if self.can_read_write:
            settings_allow_read_write_btn.image = "icon/settings.png"
            # ws_manager.send_to_all("show_hide=config_btn,1")
            ws_manager.send_to_all("reload_page")
        else:
            settings_allow_read_write_btn.image = "icon/settings_off.png"
            # ws_manager.send_to_all("show_hide=config_btn,0")
            ws_manager.send_to_all("reload_page")


class WindowManager:
    def __init__(self, window_path):
        self.window_path = window_path
        self.windows = {}
        self.active_window = self.window_path[-1]
        self.pin = ""
        self.check_lock_confirm = None
        self.calendar_diff = 0
        self.playlist_edit_mode = False

    def add_window(self, window, *args):
        if args:
            self.windows[args[0]] = window
        else:
            self.windows[window.title] = window

    def open_window(self, window_title, *args):
        self.window_path.append(window_title)
        self.active_window = self.window_path[-1]
        self.windows[window_title].show()
        self.reload_new_window(*args)
        self.windows[window_title].tk.lift()

    def close_window(self, window_title, *args):
        # self.window_path.pop(self.window_path.index(window_title))
        # self.window_path = [w for w in self.window_path if w != window_title]
        try:
            self.window_path = self.window_path[:self.window_path.index(window_title)]
        except ValueError:
            pass
        self.active_window = self.window_path[-1]
        try:
            self.reload_new_window(*args)
        except:
            pass
        self.windows[window_title].hide()

    def reload_new_window(self, *args):
        new_window = self.window_path[-1]
        if new_window == MENU:
            # generate_qrcode()
            pass
        if new_window == CALENDAR:
            self.calendar_diff = 0
            self.load_calendar()
        if new_window == PLAYLIST_VIEW:
            # if player_manager.audio_source == YOUTUBE:
            #     self.close_window(PLAYLIST_VIEW)
            #     player_manager.show_track_qrcode()
            # else:
            if player_manager.audio_source == BLUETOOTH:
                self.close_window(PLAYLIST_VIEW)
            else:
                player_manager.list_playlist()
        if new_window == RADIO_BROWSER:
            self.load_radio_browser()
        if new_window == SETTINGS_LOCK:
            self.pin = ""
            if len(args) == 1:
                settings_lock_input_txt.value = args[0]
            else:
                settings_lock_input_txt.value = ""
        if new_window == SETTINGS:
            pass
            # if SETTINGS_LOCK in self.window_path:
            #     self.window_path.pop(self.window_path.index(SETTINGS_LOCK))
            # self.windows[SETTINGS_LOCK].hide()
        if new_window == WEATHER:
            # if args[0] == 0:
            #     # pocasie_buttons_box.show()
            #     # fore_buttons_box.hide()
            #     weather_manager.weather_reload()
            # elif args[0] == 1:
            #     # fore_buttons_box.show()
            #     # pocasie_buttons_box.hide()
            weather_manager.forecast_data_update()
            weather_manager.forecast_reload()
            weather_manager.weather_reload_short()
        if new_window == FORECAST:
            weather_manager.forecast_data_update()
            weather_manager.forecast_reload()
        if new_window == RSS_VIEW:
            rss_manager.set_rss()
            rss_manager.reload_rss()
        if new_window == QR_CODE:
            if len(args) > 1:
                self.set_qrcode(args[0], args[1])
            else:
                self.set_qrcode(args[0])
        if new_window == EQUALIZER_LOW:
            if EQUALIZER_HIGH in self.window_path:
                self.window_path.pop(self.window_path.index(EQUALIZER_HIGH))
                self.windows[EQUALIZER_HIGH].hide()
            equalizer_manager.set_mixer_values(0)
        if new_window == EQUALIZER_HIGH:
            if EQUALIZER_LOW in self.window_path:
                self.window_path.pop(self.window_path.index(EQUALIZER_LOW))
                self.windows[EQUALIZER_LOW].hide()
            equalizer_manager.set_mixer_values(1)
        if new_window == AUDIO_MENU:
            pass
        if new_window == USB_VIDEO:
            if len(args) == 1:
                player_manager.list_usb_files()
        if new_window == IMAGE_VIEW:
            self.set_image_view_path(args[0])
        if new_window == BLUETOOTH_CONNECT:
            bt_devices_listbox.clear()
            if player_manager.bt_speaker_mac == "":
                bluetooth_connect_btn.image = "icon/btoff.png"
                bluetooth_connect_btn.update_command(player_manager.scan_bt_async)
        if new_window == RESTART:
            power_menu_confirm_btn.image = "icon/restart.png"
            power_menu_confirm_btn.update_command(power_restart)

            power_menu_confirm_top_btn.update_command(self.close_window, args=[RESTART])
            power_menu_confirm_middle_space0_btn.update_command(self.close_window, args=[RESTART])
            power_menu_confirm_middle_space1_btn.update_command(self.close_window, args=[RESTART])
            power_menu_confirm_bottom_btn.update_command(self.close_window, args=[RESTART])
        if new_window == SHUTDOWN:
            power_menu_confirm_btn.image = "icon/power.png"
            power_menu_confirm_btn.update_command(power_shutdown)

            power_menu_confirm_top_btn.update_command(self.close_window, args=[SHUTDOWN])
            power_menu_confirm_middle_space0_btn.update_command(self.close_window, args=[SHUTDOWN])
            power_menu_confirm_middle_space1_btn.update_command(self.close_window, args=[SHUTDOWN])
            power_menu_confirm_bottom_btn.update_command(self.close_window, args=[SHUTDOWN])
        self.reload_window()

    def reload_window(self):
        # player_manager.reload_playing_status()
        active_window = self.window_path[-1]
        if active_window == MAIN:
            main_clock_update()
        if active_window == SETTINGS:
            about_reload()
        if active_window == CLOCK:
            clock_reolad()
        if active_window == MENU:
            battery_manager.update_status()
        # if active_window == AUDIO_MENU:
        #     player_manager.audio_menu_reload()

    def pin_add(self, char):
        self.pin += char
        if '*' not in settings_lock_input_txt.value:
            settings_lock_input_txt.value = '*'
        else:
            settings_lock_input_txt.value += '*'

    def pin_remove(self):
        self.pin = ""
        settings_lock_input_txt.value = ""

    def check_lock(self):
        if self.pin == config.config['config_key']:
            self.close_window(SETTINGS_LOCK)
            self.check_lock_confirm()
            self.check_lock_confirm = None
        # elif self.pin == "%s" % time.strftime("%d%m%Y"):
        #     self.open_window(QR_CODE, "images/helo.png")
        elif self.pin == "74257655":
            self.open_window(IMAGE_VIEW, "images/rr.png")
            player_manager.youtube_search("https://www.youtube.com/watch?v=dQw4w9WgXcQ")
        elif self.pin == "":
            self.open_window(IMAGE_VIEW, "images/ysnp.jpg")
        else:
            # settings_lock_input_txt.value = "Incorrect"
            self.open_window(IMAGE_VIEW, "images/ysnp.jpg")
        self.pin = ''

    def check_config_lock(self):
        if config.config['config_key'] != "" and not config.can_read_write:
            self.check_lock_confirm = config.toggle_allow_read_write
            self.open_window(SETTINGS_LOCK)
        else:
            config.toggle_allow_read_write()

    def set_qrcode(self, data, text=None):
        qr_code_picture.width = 240
        qr_code_picture.height = 240
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=5,
            border=1,
        )
        qr.add_data(data)
        qr.make(fit=True)
        qrcode_image = qr.make_image(fill_color="black", back_color="white")
        qr_code_picture.image = qrcode_image
        qr_code_text.value = data if text is None else text

        # img = ImageTk.PhotoImage(file=data)
        # qr_code_picture.width = img.width()
        # qr_code_picture.height = img.height()
        #
        # qr_code_picture.image = img
        # qr_code_text.value = ""

    def open_browser(self):
        subprocess.run(['sh', 'startbro.sh', qr_code_text.value])

    def toggle_volume_slider(self):
        if volume_slider.visible:
            volume_slider.hide()
            volume_slider.height = 0
            position_txt.show()
        else:
            position_txt.hide()
            volume_slider.height = 24
            volume_slider.show()

    def toggle_playlist_edit_mode(self):
        if self.playlist_edit_mode:
            playlist_remove_btn.hide()
            playlist_qrcode_btn.show()
            move_track_up_btn.hide()
            move_track_down_btn.hide()
            playlist_reload_btn.hide()
            playlist_open_browser_btn.show()
        else:
            playlist_remove_btn.show()
            playlist_qrcode_btn.hide()
            move_track_up_btn.show()
            move_track_down_btn.show()
            playlist_reload_btn.show()
            playlist_open_browser_btn.hide()
        self.playlist_edit_mode = not self.playlist_edit_mode

    def toggle_rss_edit_mode(self):
        if reload_rss_links_btn.visible:
            reload_rss_links_btn.hide()
            move_rss_link_up_btn.hide()
            move_rss_link_down_btn.hide()
            rss_open_btn.show()
        else:
            reload_rss_links_btn.show()
            move_rss_link_up_btn.show()
            move_rss_link_down_btn.show()
            rss_open_btn.hide()

    def show_device_ip_qrcode(self):
        ipaddress = subprocess.run(["/bin/hostname", "-I"], check=True, stdout=subprocess.PIPE).stdout.decode('utf-8').strip()
        if ipaddress == "10.41.0.1":
            window_manager.open_window(
                QR_CODE,
                "WIFI:T:nopass;S:Raπdio;;",
                "Connect to \"Raπdio\" WiFi AP"
            )
        else:
            window_manager.open_window(
                QR_CODE,
                "http://" + subprocess.run(["/bin/hostname", "-I"], check=True,
                                           stdout=subprocess.PIPE).stdout.decode('utf-8').strip().split(" ")[-1] + ":8080"
            )

    def set_image_view_path(self, image):
        # img = ImageTk.PhotoImage(file=image)
        # aspect_ratio = img.height() / img.width()
        #
        # new_width = 480
        # new_height = new_width * aspect_ratio
        #
        # resized_image = orig_image.resize((new_width, new_height), Image.ANTIALIAS)
        # qr_code_picture.image = resized_image

        # img = ImageTk.PhotoImage(file=image)
        # # qr_code_picture.width = img.width()
        # # qr_code_picture.height = img.height()
        # qr_code_picture.image = image.resize((480, 320))

        img = Image.open(image)
        img = resizeimage.resize_contain(img, [480, 320])

        image_view_picture.image = img

    def close_image_view(self):
        window_manager.close_window(IMAGE_VIEW)
        image_view_picture.image = "icon/nic.png"

    def load_radio_browser(self):
        radio_browser_listbox.clear()
        for item in RADIO_BROWSER_FIRST:
            radio_browser_listbox.append(item)
        player_manager.radio_browser_state = [RBS_FIRST]
        radio_browser_next_btn.show()
        radio_browser_prev_btn.hide()
        radio_browser_play_btn.hide()
        radio_browser_add_btn.hide()
        radio_browser_sort_btn.hide()

    def load_calendar(self):
        month = subprocess.run(["date", "-d", f"+{self.calendar_diff} month", "+%m %Y"], check=True,
                               stdout=subprocess.PIPE).stdout.decode('utf-8').strip().split(" ")
        calendar_output = subprocess.run(["ncal", "-M", "-b"] + month, check=True, stdout=subprocess.PIPE).stdout.decode('utf-8')\
            .rstrip().replace("  \n", " \n ").split("\n", maxsplit=1)
        calendar_month_txt.value = calendar_output[0].strip()
        calendar_line = calendar_output[1].replace(" _\x08", "|", 1)
        day = calendar_line.find("_\x08") + 3  # if not found `find` return -1, 3 is added, so if not found its 2
        calendar_txt.value = calendar_line[:day].replace("_\x08", "") + "|" + calendar_line[day+1:] if day != 2 else calendar_line

    def calendar_change_diff(self, diff):
        self.calendar_diff += diff
        self.load_calendar()


class RssManager:
    def __init__(self):
        self.rss_data = {}
        self.rss_now = -1
        self.rss_old = -1
        self.rss_count = -1
        self.rss_link = ""

        self.rss_links = config.config['rss_links'].copy()
        self.rss_names = []
        self.rss_urls = []

    def reload_links(self):
        rss_links_errors_txt.value = "Loading"
        rss_open_btn.image = "icon/back180_off.png"
        self.rss_links = config.config['rss_links'].copy()
        self.rss_names = []
        self.rss_urls = []
        self.add_items()
        ws_manager.send_to_all(self.get_rss_links_json())

    def reload_links_async(self):
        threading.Thread(target=self.reload_links, name="rss_manager.reload_links_async", daemon=True).start()

    def open_rss(self):
        if rss_links_listbox.value is not None:
            self.set_rss()
            window_manager.open_window(RSS_VIEW)

    def add_items(self):
        rss_links_listbox.clear()
        errors = 0
        rss_links_broken = []
        for link in self.rss_links:
            try:
                title = feedparser.parse(link)['feed']['title']
                url = feedparser.parse(link)['feed']['link']
                self.rss_names.append(title)
                self.rss_urls.append(url)
                rss_links_listbox.append(title)
            except OSError:
                self.rss_names.append(None)
                self.rss_urls.append(None)
                rss_links_broken.append(link)
                errors += 1
            except KeyError:
                return

        if errors > 0:
            for link in rss_links_broken:
                self.rss_links.remove(link)
            rss_links_errors_txt.value = "Error\n" + str(errors) + " feed" + ("s" if errors > 1 else "")
        else:
            rss_links_errors_txt.value = ""

    def reload_rss(self):
        if self.rss_now != self.rss_old:
            self.rss_data = feedparser.parse(self.rss_link)

        rss_title_txt.value = str(self.rss_data.entries[self.rss_count].title)
        description = str(self.rss_data.entries[self.rss_count].description)
        if len(description) > 512:
            rss_description_txt.value = description[:512] + "..."
        else:
            rss_description_txt.value = description
        rss_date_txt.value = str(parser.parse(self.rss_data.entries[self.rss_count].published))[:-6]

        self.rss_old = self.rss_now

    def set_rss(self):
        self.rss_count = 0
        link_index = rss_links_listbox.items.index(rss_links_listbox.value)
        self.rss_link = self.rss_links[link_index]
        self.rss_now = self.rss_link

    def next_rss(self):
        self.rss_count += 1
        self.reload_rss()

    def prev_rss(self):
        self.rss_count -= 1
        self.reload_rss()

    def show_rss_qrcode(self):
        window_manager.open_window(QR_CODE, str(self.rss_data.entries[self.rss_count].link))

    def get_rss_links_json(self):
        rss_links = config.config['rss_links']

        try:
            rss_items = []
            for index, link in enumerate(rss_links):
                rss_item = {
                    "id": str(index),
                    "name": self.rss_names[index] if self.rss_names[index] is not None else link,
                    "url": self.rss_urls[index],
                    "works": True if link in self.rss_links else False
                }
                rss_items.append(rss_item)
        except IndexError:
            rss_items = [{
                    "id": -1,
                    "name": "Loading...",
                    "url": None,
                    "works": True
                }]

        json_data = {
            "type": "data",
            "data": "rss",
            "value": rss_items
        }

        return json.dumps(json_data)

    def rss_link_selected(self):
        rss_open_btn.image = "icon/back180.png"


class EqualizerManager:
    def __init__(self, control_boxes, sliders, buttons_boxes, buttons):
        self.control_boxes = control_boxes
        self.sliders = sliders
        self.buttons_boxes = buttons_boxes
        self.buttons = buttons

    def get_mixer_value(self, name):
        try:
            mixer = alsaaudio.Mixer(name, **MIXER_DEVICE)
            volume = mixer.getvolume()[0]
            return str(volume)
        except alsaaudio.ALSAAudioError:
            return ""

    def set_mixer_values(self, page):
        for btn in range(page * 5, page * 5 + 5):
            val = self.get_mixer_value(EQUAL_CONTROLS[btn])
            self.buttons[btn][1].value = val
            self.sliders[btn].value = -int(val)

    def set_mixer_value(self, volume, name):
        try:
            mixer = alsaaudio.Mixer(name, **MIXER_DEVICE)
        except alsaaudio.ALSAAudioError:
            return
        mixer.setvolume(int(volume), alsaaudio.MIXER_CHANNEL_ALL)
        ws_manager.send_to_all("eq_" + str(EQUAL_CONTROLS.index(name)) + "=" + str(volume))

    def change_mixer(self, slider_value, slider_index):
        if slider_value == '+':
            slider_value = int(self.get_mixer_value(EQUAL_CONTROLS[slider_index])) + 2
        if slider_value == '-':
            slider_value = int(self.get_mixer_value(EQUAL_CONTROLS[slider_index])) - 2

        slider_value = abs(int(slider_value))
        self.set_mixer_value(slider_value, EQUAL_CONTROLS[slider_index])

        self.buttons[slider_index][1].value = str(slider_value)
        self.sliders[slider_index].value = -slider_value

    def equalizer_save(self):
        equalizer_preset = "".join(
            self.get_mixer_value(EQUAL_CONTROLS[ec]) + ":" for ec in range(9)
        )
        equalizer_preset += self.get_mixer_value(EQUAL_CONTROLS[9])

        with open(config.file_path) as file:
            config_file = yaml.load(file, Loader=yaml.SafeLoader)
        config_file['equalizer'] = equalizer_preset
        with open(config.file_path, "w") as file:
            yaml.dump(config_file, file)

    def equalizer_load(self, page=-1):
        with open(config.file_path) as file:
            config_file = yaml.load(file, Loader=yaml.SafeLoader)
        equalizer_preset = config_file['equalizer'].split(":")
        for ec in range(10):
            self.set_mixer_value(equalizer_preset[ec], EQUAL_CONTROLS[ec])
        self.set_mixer_values(page)


class PlayerManager:
    def __init__(self, radio_browser, audio_source, is_playing, info_lines, volume_step, youtube_max_results):
        self.radio_browser = radio_browser
        self.next_btn_is_pressed = False
        self.prev_btn_is_pressed = False
        self.audio_source = audio_source
        self.is_playing = is_playing
        self.info_lines = info_lines
        self.volume_step = volume_step
        self.youtube_url = None
        self.yt_res_html = ""
        self.youtube_playlist = []
        self.youtube_max_results = youtube_max_results
        self.previous_volume = 0
        self.bt_pairing = False
        self.can_start_bt = False
        self.bt_devices = {}
        self.bt_connecting_to_speaker = False
        self.bt_speaker_name = ""
        self.bt_speaker_mac = ""
        self.manual_disconnect = False
        self.player_interface = None
        self.property_interface = None
        self.bluetooth_device_name = ""
        self.bluetooth_device_mac = ""
        self.usb_drive = None
        self.playlist_confirm_remove_timer = None

        self.radio_browser_state = []
        self.radio_browser_first = ""
        self.radio_browser_first_values_sort_by_name = True
        self.radio_browser_first_value = ""
        self.radio_browser_first_values = []
        self.radio_browser_sort = ""
        self.radio_browser_sort_value = ""
        self.radio_browser_results = []

        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        self.bus = dbus.SystemBus()
        self.obj = self.bus.get_object('org.bluez', "/")
        self.mgr = dbus.Interface(self.obj, 'org.freedesktop.DBus.ObjectManager')
        self.transport_prop_iface = None
        for path, ifaces in self.mgr.GetManagedObjects().items():
            if 'org.bluez.MediaPlayer1' in ifaces:
                self.player_interface = dbus.Interface(
                    self.bus.get_object('org.bluez', path),
                    'org.bluez.MediaPlayer1')
            elif 'org.bluez.MediaTransport1' in ifaces:
                self.transport_prop_iface = dbus.Interface(
                    self.bus.get_object('org.bluez', path),
                    'org.freedesktop.DBus.Properties')
        self.property_interface = dbus.Interface(self.player_interface,
                                                 dbus_interface='org.freedesktop.DBus.Properties')

        self.bus.add_signal_receiver(
            self.on_property_changed,
            bus_name='org.bluez',
            signal_name='PropertiesChanged',
            dbus_interface='org.freedesktop.DBus.Properties')

        threading.Thread(target=GLib.MainLoop().run, name="GLib.MainLoop().run", daemon=True).start()

    def on_property_changed(self, interface, changed, invalidated):
        print(interface)
        # if self.bt_connecting_to_speaker:
        #     print("connecting to speaker skipping...")
        #     return
        # if not interface.startswith("org.bluez.MediaPlayer"):
        #     print("IGNORE THIS")
        #     return

        for prop, value in changed.items():
            print(prop, value)
            if prop == 'Status':
                if self.audio_source == BLUETOOTH:
                    self.reload_playing_status(value)
            elif prop == 'Track' or prop == 'Metadata':
                if self.audio_source == BLUETOOTH:
                    artist = value.get('Artist', '')
                    title = value.get('Title', '')
                    album = value.get('Album', '')
                    self.blue_update_info([artist, album, title])
            elif prop == 'Connected':
                if interface.startswith("org.bluez.MediaControl"):
                    continue
                if value and player_manager.audio_source != BLUETOOTH:
                    # subprocess.run(["/usr/bin/sudo", "systemctl", "restart", "mpd"])
                    self.can_start_bt = True
                elif not value and player_manager.audio_source == BLUETOOTH:
                    hciout = subprocess.run(["/usr/bin/hcitool", "con"], check=True,
                                            stdout=subprocess.PIPE).stdout.decode('utf-8')
                    if self.bluetooth_device_mac.replace("_", ":") not in hciout:
                        self.can_start_bt = False
                        self.start_radio()
                elif not value and not self.manual_disconnect and interface.startswith("org.bluez.Device"):
                    hciout = subprocess.run(["/usr/bin/hcitool", "con"], check=True,
                                            stdout=subprocess.PIPE).stdout.decode('utf-8')
                    if self.bt_speaker_mac not in hciout:
                        is_playing = bool(self.is_playing)
                        self.disconnected_bt_speaker(is_playing)

            elif prop == 'Player':
                # if self.audio_source != BLUETOOTH and self.can_start_bt:
                if self.can_start_bt:
                    self.bluetooth_device_mac = re.search(r'(([0-9A-F]{2}_){5})[0-9A-F]{2}', value).group()
                    bluetooth_device_name_cmd = subprocess.run(["/usr/bin/hcitool",
                                                                "name",
                                                                self.bluetooth_device_mac.replace("_", ":")],
                                                               check=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
                    self.bluetooth_device_name = bluetooth_device_name_cmd.strip()
                    print("Player connected", self.bluetooth_device_name, self.bluetooth_device_mac)
                    self.player_interface = dbus.Interface(
                        self.bus.get_object('org.bluez', value),
                        'org.bluez.MediaPlayer1')
                    self.start_bluetooth(False)
                else:
                    self.can_start_bt = False

    def next_btn_pressed(self):
        time.sleep(0.3)
        if next_btn.value:
            next_btn.update_command(None)
            self.seek("+5")
            self.next_btn_pressed()

    def next_btn_released(self):
        time.sleep(0.1)
        next_btn.update_command(self.next_track)

    def prev_btn_pressed(self):
        time.sleep(0.3)
        if prev_btn.value:
            prev_btn.update_command(None)
            self.seek("-5")
            self.prev_btn_pressed()

    def prev_btn_released(self):
        time.sleep(0.1)
        prev_btn.update_command(self.prev_track)

    def next_track(self):
        if self.audio_source == BLUETOOTH:
            try:
                self.player_interface.Next()
            except (dbus.exceptions.DBusException, AttributeError):
                print("error bluetooth next")
        # elif self.audio_source == YOUTUBE:
        #    self.start_radio()
        else:
            subprocess.run([MPC_PATH, "next"])

    def prev_track(self):
        if self.audio_source == BLUETOOTH:
            try:
                self.player_interface.Previous()
            except (dbus.exceptions.DBusException, AttributeError):
                print("error bluetooth previous")
        # elif self.audio_source == YOUTUBE:
        #    self.start_radio()
        else:
            subprocess.run([MPC_PATH, "prev"])

    def play_stop(self):
        if self.is_playing:
            if self.audio_source == BLUETOOTH:
                try:
                    self.player_interface.Stop()
                except (dbus.exceptions.DBusException, AttributeError):
                    print("error bluetooth stop")
            else:
                subprocess.run([MPC_PATH, "stop" if self.audio_source == RADIO else "pause"])

        else:
            if self.audio_source == BLUETOOTH:
                try:
                    self.player_interface.Play()
                except (dbus.exceptions.DBusException, AttributeError):
                    print("error bluetooth play")
            else:
                subprocess.run([MPC_PATH, "play"])

        self.is_playing = not self.is_playing

    def seek(self, value):
        if self.audio_source == USB or self.audio_source == YOUTUBE:
            subprocess.run([MPC_PATH, "seek", value])

    def start_radio(self, skip_stop=False):
        is_playing = bool(self.is_playing)

        if self.audio_source == RADIO:
            return
        elif skip_stop:
            pass
        elif self.audio_source == USB:
            self.stop_usb()
        elif self.audio_source == BLUETOOTH:
            self.stop_bluetooth()
        elif self.audio_source == YOUTUBE:
            self.stop_youtube()

        self.audio_source = RADIO
        playlist_edit_mode_btn.show()
        playlist_open_browser_btn.show()
        subprocess.run([MPC_PATH, "clear"])
        subprocess.run([MPC_PATH, "random", "off"])
        subprocess.run([MPC_PATH, "repeat", "on"])
        subprocess.run([MPC_PATH, "update"])
        subprocess.run([MPC_PATH, "load", "playlist"])
        if is_playing:
            subprocess.run([MPC_PATH, "play"])
        else:
            subprocess.run([MPC_PATH, "stop"])
        play_radio_btn.image = "icon/radio.png"
        self.reload_playlist()

    def stop_radio(self):
        playlist_edit_mode_btn.hide()
        playlist_open_browser_btn.hide()
        play_radio_btn.image = "icon/radio_off.png"

    def start_usb(self):
        if self.audio_source == USB:
            return
        elif self.audio_source == RADIO:
            self.stop_radio()
        elif self.audio_source == BLUETOOTH:
            self.stop_bluetooth()
        elif self.audio_source == YOUTUBE:
            self.stop_youtube()

        self.audio_source = USB
        if window_manager.playlist_edit_mode:
            window_manager.toggle_playlist_edit_mode()
        playlist_qrcode_btn.hide()
        blkid_out = subprocess.run(["/usr/sbin/blkid"], check=True,
                                   stdout=subprocess.PIPE).stdout.decode('utf-8').split("\n")
        self.usb_drive = None
        for line in blkid_out:
            if re.fullmatch(re.compile("^(/dev/sd).*$"), line):
                self.usb_drive = line.split(":", maxsplit=1)[0]
                break
        print(self.usb_drive)

        if self.usb_drive is None:
            lsblk_out = subprocess.run(["/usr/bin/lsblk"], check=True,
                                       stdout=subprocess.PIPE).stdout.decode('utf-8').split("\n")
            print(lsblk_out)
            for line in lsblk_out:
                if re.fullmatch(re.compile("^..+(sd)[a-z][0-9].*$"), line):
                    self.usb_drive = "/dev/sd" + line.split("sd")[1][0:2]
                    break

            print(self.usb_drive)

            if self.usb_drive is None:
                self.start_radio(True)
                return

        subprocess.run(["/usr/bin/sudo", "/bin/mount", self.usb_drive, "/mnt/usbdrive"])
        subprocess.run([MPC_PATH, "clear"])
        subprocess.run([MPC_PATH, "random", "off"])
        subprocess.run([MPC_PATH, "repeat", "on"])
        subprocess.run([MPC_PATH, "update"])
        time.sleep(1)
        subprocess.run([MPC_PATH, "add", "/"])
        self.reload_playlist()
        usb_video_btn.image = "icon/folder.png"
        mp3_btn.image = "icon/usb.png"

    def stop_usb(self):
        playlist_qrcode_btn.show()
        subprocess.run([MPC_PATH, "stop"])
        # subprocess.run([MPC_PATH, "clear"])
        subprocess.run(["/bin/fuser", "--kill", "/mnt/usbdrive"])
        subprocess.run(["/usr/bin/sudo", "/bin/umount", self.usb_drive])
        usb_video_btn.image = "icon/folder_off.png"
        mp3_btn.image = "icon/usb_off.png"

    def start_bluetooth(self, pair=True):
        if self.audio_source == BLUETOOTH:
            return
        elif self.audio_source == RADIO:
            self.stop_radio()
        elif self.audio_source == USB:
            self.stop_usb()
        elif self.audio_source == YOUTUBE:
            self.stop_youtube()

        self.audio_source = BLUETOOTH
        bluetooth_btn.image = "icon/bton.png"
        self.previous_volume = self.get_volume_value()
        subprocess.run([MPC_PATH, "stop"])
        self.audio_menu_reload()

        # not_connected = False if "> ACL" in subprocess.run(["hcitool", "con"],
        #                                                    check=True,
        #                                                    stdout=subprocess.PIPE).stdout.decode('utf-8') else True

        # if pair or not_connected:
        #     self.bt_pairing = True
        #     threading.Thread(target=subprocess.run, args=[["/bin/bash", "pair_and_trust_bluetooth_device.sh"]],
        #                      name="start_bluetooth pair_and_trust_bluetooth_device", daemon=True).start()
        threading.Thread(target=subprocess.run, name="bluealsa-aplay",
                         args=[["/usr/bin/bluealsa-aplay",
                                "-d",
                                "equal" if self.bt_speaker_mac == "" else "bt-out",
                                "00:00:00:00:00:00"]], daemon=True).start()
        self.reload_playlist()
        try:
            btinfo = self.property_interface.Get('org.bluez.MediaPlayer1', 'Track')
            btinfo = json.dumps(btinfo)
            btinfo = json.loads(btinfo)

            self.info_lines[0] = btinfo["Artist"]
            self.info_lines[1] = btinfo["Album"]
            self.info_lines[2] = btinfo["Title"]

            info_lines_txt[0].value = self.info_lines[0]
            info_lines_txt[1].value = self.info_lines[1]
            info_lines_txt[2].value = self.info_lines[2]
        except AttributeError:
            pass

    def stop_bluetooth(self):
        # self.volume_control("", str(self.previous_volume))
        try:
            self.player_interface.Pause()
        except (dbus.exceptions.DBusException, AttributeError):
            print("error bluetooth pause player")
        subprocess.run(["/usr/bin/pkill", "-f", "bluealsa-aplay"])
        self.bluetooth_device_mac = ""
        self.bluetooth_device_name = ""
        bluetooth_device_name_txt.value = ""
        self.volume_reload()
        bluetooth_btn.image = "icon/btoff.png"

    def start_youtube(self):
        if self.audio_source == YOUTUBE:
            return
        elif self.audio_source == RADIO:
            self.stop_radio()
        elif self.audio_source == USB:
            self.stop_usb()
        elif self.audio_source == BLUETOOTH:
            self.stop_bluetooth()

        subprocess.run([MPC_PATH, "clear"])
        subprocess.run([MPC_PATH, "repeat", "off"])
        self.audio_source = YOUTUBE
        # self.reload_playlist()

    def stop_youtube(self):
        self.youtube_url = None
        self.youtube_playlist = []

    # def switch_music(self):
    #     is_playing = bool(self.is_playing)
    #     if self.audio_source == RADIO:
    #         playlist_edit_mode_btn.hide()
    #         playlist_qrcode_btn.hide()
    #         subprocess.run(["/usr/bin/sudo", "/bin/mount", "/dev/sda1", "/mnt/usbdrive"])
    #         subprocess.run([MPC_PATH, "clear"])
    #         subprocess.run([MPC_PATH, "random", "off"])
    #         subprocess.run([MPC_PATH, "repeat", "on"])
    #         subprocess.run([MPC_PATH, "update"])
    #         time.sleep(1)
    #         subprocess.run([MPC_PATH, "add", "/"])
    #         mp3_btn.image = "icon/usb.png"
    #         self.audio_source = USB
    #
    #     if is_playing:
    #         subprocess.run([MPC_PATH, "play"])
    #     else:
    #         subprocess.run([MPC_PATH, "stop"])
    #     self.reload_playlist()

    def shuffle(self):
        shuffle_str = subprocess.run([MPC_PATH, "status"], check=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
        if "random: on" in shuffle_str:
            subprocess.run([MPC_PATH, "random", "off"])
            random_play_btn.image = "icon/shuffleoff.png"
        elif "random: off" in shuffle_str:
            subprocess.run([MPC_PATH, "random", "on"])
            random_play_btn.image = "icon/shuffleon.png"

    # not used
    # def get_btdev_name(self):
    #     bluetooth_device_name_cmd = subprocess.run(
    #         ["/usr/bin/hcitool", "name", self.bluetooth_device_mac[4:-8].replace("_", ":")],
    #         check=True, stdout=subprocess.PIPE).stdout.decode('utf-8')[:-1]
    #     if "Usage" not in bluetooth_device_name_cmd:
    #         self.bluetooth_device_name = bluetooth_device_name_cmd
    #     else:
    #         self.bluetooth_device_name = None

    # def switch_bluetooth(self, pair=True):
    #     if self.audio_source == RADIO:
    #         self.audio_source = BLUETOOTH
    #         self.bt_pairing = True
    #         bluetooth_btn.image = "icon/bton.png"
    #         # volume_icon_img.visible = False
    #         # volume_data_txt.visible = False
    #         # volume_down_btn.image = "icon/nic.png"
    #         # volume_up_btn.image = "icon/bton.png"
    #         # volume_up_btn.update_command(self.switch_bluetooth)
    #         self.previous_volume = self.get_volume_value()
    #         subprocess.run([MPC_PATH, "stop"])
    #         self.audio_menu_reload()
    #         if pair:
    #             threading.Thread(target=subprocess.run, args=[["/bin/bash", "pair_and_trust_bluetooth_device.sh"]],
    #                              name="switch_bluetooth pair_and_trust_bluetooth_device", daemon=True).start()
    #         self.audio_menu_reload()
    #         threading.Thread(target=subprocess.run, name="bluealsa-aplay",
    #                          args=[["/usr/bin/bluealsa-aplay", "-d", "equal", "00:00:00:00:00:00"]],daemon=True).start()
    #
    #         btinfo = self.property_interface.Get('org.bluez.MediaPlayer1', 'Track')
    #         btinfo = json.dumps(btinfo)
    #         btinfo = json.loads(btinfo)
    #
    #         self.info_lines[0] = btinfo["Artist"]
    #         self.info_lines[1] = btinfo["Album"]
    #         self.info_lines[2] = btinfo["Title"]
    #
    #         info_lines_txt[0].value = self.info_lines[0]
    #         info_lines_txt[1].value = self.info_lines[1]
    #         info_lines_txt[2].value = self.info_lines[2]
    #
    #     elif self.audio_source == BLUETOOTH:
    #         self.audio_source = RADIO
    #         bluetooth_btn.image = "icon/btoff.png"
    #         # volume_icon_img.visible = True
    #         # volume_data_txt.visible = True
    #         # volume_down_btn.image = "icon/volume_down.png"
    #         # volume_up_btn.image = "icon/volume_up.png"
    #         # volume_up_btn.update_command(self.volume_control, args=["+"])
    #         self.volume_control("", str(self.previous_volume))
    #         if self.is_playing:
    #             subprocess.run([MPC_PATH, "play"])
    #         try:
    #             self.player_interface.Pause()
    #         except (dbus.exceptions.DBusException, AttributeError):
    #             print("error bluetooth pause player")
    #         subprocess.run(["/usr/bin/pkill", "-f", "bluealsa-aplay -d equal 00:00:00:00:00:00"])
    #         self.volume_reload()

    def reload_playing_status(self, value=False):
        if self.audio_source != BLUETOOTH:
            status = subprocess.run([MPC_PATH, "status"], check=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
        else:
            status = value
        self.is_playing = "playing" in status
        icon = stop_icon if self.is_playing else play_icon
        if play_stop_btn.image != icon:
            play_stop_btn.image = icon
        ws_manager.send_data_to_all("player", {"type": "status", "data": str(int(self.is_playing))})

    def audio_menu_reload(self):
        if self.audio_source == BLUETOOTH:
            if self.bluetooth_device_mac != "":
                bluetooth_btn.image = "icon/btcon.png"
                bluetooth_device_name_txt.value = self.bluetooth_device_name
                self.bt_pairing = False
            else:
                hci_out = subprocess.run(["hcitool", "con"], check=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
                if "> ACL" in hci_out:
                    bluetooth_btn.image = "icon/btcon.png"
                    bluetooth_device_name_txt.value = "Device connected"
                else:
                    bluetooth_btn.image = "icon/bton.png"
                    bluetooth_device_name_txt.value = "No device connected"

        else:
            bluetooth_btn.image = "icon/btoff.png"
            bluetooth_device_name_txt.value = ""
            # if self.audio_source == USB:
            #     mp3_btn.image = "icon/radio.png"
            # else:
            #     mp3_btn.image = "icon/usb.png"

    def info_reload(self):
        if self.audio_source == RADIO:
            output = subprocess.run([MPC_PATH, "current", "-f", "%name%\\]\\[%title%"], check=True,
                                    stdout=subprocess.PIPE).stdout.decode('utf-8').strip().split("][")
            self.info_lines[0] = output[0]
            if self.info_lines[0] == "":
                self.info_lines[0] = "Rádio"
            # if self.info_lines[0] == "":
            #     self.info_lines[0] = subprocess.run([MPC_PATH, "current", "-f", "%artist%"], check=True,
            #                                         stdout=subprocess.PIPE).stdout.decode('utf-8').strip()
            if len(output) > 1:
                info_lines = output[1]

                if " - " in info_lines:
                    lines = info_lines.split(" - ", maxsplit=1)
                else:
                    lines = info_lines.split("-", maxsplit=1)

                if len(lines) == 2:
                    self.info_lines[1] = lines[0].strip()
                    self.info_lines[2] = lines[1].strip()
                elif len(lines) == 1:
                    self.info_lines[1] = lines[0].strip()
                    self.info_lines[2] = " "
            else:
                self.info_lines[1] = " "
                self.info_lines[2] = " "

        if self.audio_source == USB:
            output = subprocess.run([MPC_PATH, "current", "-f",
                                     "[%artist%\\]\\[%album%\\]\\[%title%]|[USB\\]\\[ \\]\\[%file%]"],
                                    check=True, stdout=subprocess.PIPE).stdout.decode('utf-8').strip().split("][")
            self.info_lines = output

        if self.audio_source == YOUTUBE:
            try:
                index = int(subprocess.run([MPC_PATH, "current", "-f", "%position%"], check=True,
                                           stdout=subprocess.PIPE).stdout.decode('utf-8').strip()) - 1
                self.info_lines[0] = self.youtube_playlist[index]['0']
                self.info_lines[1] = self.youtube_playlist[index]['1']
                self.info_lines[2] = self.youtube_playlist[index]['2']
                self.youtube_url = self.youtube_playlist[index]['url']
            except ValueError:
                pass

        if len(self.info_lines) == 3:
            for line in range(3):
                if info_lines_txt[line].value != self.info_lines[line]:
                    info_lines_txt[line].value = self.info_lines[line]

            self.send_infolines()
        else:
            self.info_lines = ["", "", ""]
            for line in range(3):
                info_lines_txt[line].value = self.info_lines[line]

            self.send_infolines()

    def blue_update_info(self, data):
        self.info_lines[0] = data[0]
        self.info_lines[1] = data[1]
        self.info_lines[2] = data[2]

        info_lines_txt[0].value = self.info_lines[0]
        info_lines_txt[1].value = self.info_lines[1]
        info_lines_txt[2].value = self.info_lines[2]

        self.send_infolines()

    def send_infolines(self):
        ws_manager.send_data_to_all("player", {"type": "infolines", "data": [{"info1": self.info_lines[0]},
                                                                             {"info2": self.info_lines[1]},
                                                                             {"info3": self.info_lines[2]}]})

    def get_volume_value(self):
        volume_cmd_out = subprocess.run([MPC_PATH, "volume"], check=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
        return int(re.search(r'\d+', volume_cmd_out).group())

    @Throttle(milliseconds=100)
    def volume_img_reload(self, volume_value):
        if volume_value % 2 != 0:
            volume_value += 1
        volume_icon_img.image = "icon/vol" + str(volume_value) + ".png"

    def volume_reload(self):
        # if self.audio_source != BLUETOOTH:
        volume_value = self.get_volume_value()

        # if (volume_value % 2) != 0:
        #     volume_value += 1
        #     subprocess.run([MPC_PATH, "vol", str(volume_value)])

        if volume_data_txt.value != str(volume_value):
            volume_data_txt.value = str(volume_value) + "%"
            self.volume_img_reload(volume_value)
            volume_slider.value = volume_value
            ws_manager.send_data_to_all("player", {"type": "volume", "data": str(volume_value)})

    def volume_control(self, state="", value=None):
        subprocess.run([MPC_PATH, "vol", state + (value if value is not None else self.volume_step)])
        if self.audio_source == BLUETOOTH:
            try:
                bt_volume = (self.get_volume_value() / 100) * 127
                self.transport_prop_iface.Set(
                    'org.bluez.MediaTransport1',
                    'Volume',
                    dbus.UInt16(bt_volume))
            except AttributeError:
                pass
        # self.volume_reload()

    def reload_playlist(self, index=None):
        is_playing = self.is_playing
        if index is None:
            if self.audio_source == RADIO:
                try:
                    index = int(subprocess.run([MPC_PATH, "current", "-f", "%position%"], check=True,
                                               stdout=subprocess.PIPE).stdout.decode('utf-8').strip()) - 1
                    length = len(subprocess.run([MPC_PATH, "playlist", "playlist"], check=True,
                                                stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')) - 1
                    print("index:", index, "length:", length)
                    if index == length:
                        index -= 1
                except ValueError:
                    index = 0
            else:
                index = 0

        if self.audio_source == RADIO:
            subprocess.run([MPC_PATH, "clear"])
            subprocess.run([MPC_PATH, "update"])
            subprocess.run([MPC_PATH, "load", "playlist"])
            if is_playing:
                subprocess.run([MPC_PATH, "play", str(int(index) + 1)])

        self.list_playlist(index)
        ws_manager.send_to_all(self.get_playlist_json())

    def list_playlist(self, index=None):
        playlist_view_listbox.clear()
        if self.audio_source not in [YOUTUBE, BLUETOOTH]:
            if self.audio_source == RADIO:
                command = [MPC_PATH, "playlist", "playlist"]
            else:
                command = [MPC_PATH, "playlist", "-f", "[%title% - %artist%]|%file%"]
            playlist_out = subprocess.run(command,
                                          check=True, stdout=subprocess.PIPE).stdout.decode('utf-8').strip().split("\n")
            for track in playlist_out:
                if not track.startswith("http"):
                    playlist_view_listbox.append(track[:28])

            playing_track_position = subprocess.run([MPC_PATH, "current", "-f", "%position%"], check=True,
                                                    stdout=subprocess.PIPE).stdout.decode('utf-8').strip()

            if index is None:
                selected = int(playing_track_position) - 1
            else:
                selected = int(index)

            try:
                playlist_view_listbox.value = playlist_view_listbox.items[selected]
                playlist_view_listbox.children[0].tk.see(selected)
            except IndexError:
                pass

        elif self.audio_source == YOUTUBE:
            for track in self.youtube_playlist:
                playlist_view_listbox.append(track['2'][:28])

            playing_track_position = int(subprocess.run([MPC_PATH, "current", "-f", "%position%"], check=True,
                                                        stdout=subprocess.PIPE).stdout.decode('utf-8').strip()) - 1
            try:
                playlist_view_listbox.value = playlist_view_listbox.items[playing_track_position]
                playlist_view_listbox.children[0].tk.see(playing_track_position)
            except (IndexError, ValueError):
                pass

    def play_track(self, track_id):
        if self.audio_source in [RADIO, USB, YOUTUBE]:
            subprocess.run([MPC_PATH, "play", str(int(track_id) + 1)])

    def play_track_by_name(self, track_name):
        # heh
        self.play_track(playlist_view_listbox.items.index(track_name[:28]))

    def play_selected_track(self):
        selected_index = playlist_view_listbox.items.index(playlist_view_listbox.value)
        self.play_track(selected_index)

    def get_playlist_json(self):
        if self.audio_source != BLUETOOTH:
            if self.audio_source != YOUTUBE:
                command = [MPC_PATH, "playlist", "-f", "%name%|[%title% - %artist%]|%file%"]
                if self.audio_source == RADIO:
                    command.append("playlist")
                playlist_out = subprocess.run(command, check=True,
                                              stdout=subprocess.PIPE).stdout.decode('utf-8').strip().split("\n")
                playlist_urls_out = subprocess.run([MPC_PATH, "playlist", "-f", "%file%"], check=True,
                                                   stdout=subprocess.PIPE).stdout.decode('utf-8').strip().split("\n")
            elif self.audio_source == YOUTUBE:
                playlist_out = playlist_view_listbox.items

            playlist_tracks = []
            for index, track in enumerate(playlist_out):
                track_url = ""
                if self.audio_source != YOUTUBE:
                    track_url = playlist_urls_out[index]
                elif self.audio_source == YOUTUBE:
                    track_url = self.youtube_playlist[index]['url']

                track_item = {
                    "id": str(index),
                    "name": track,
                    "url": track_url
                }
                playlist_tracks.append(track_item)

            json_data = {
                "type": "data",
                "data": "playlist",
                "value": playlist_tracks
            }

            return json.dumps(json_data)
        else:
            return "Playing from bluetooth"

    def add_to_playlist(self, name, url, reload=True):
        with open("playlists/playlist.m3u", "a") as playlist_file:
            playlist_file.write("#EXTINF:0," + name.strip() + "\n")
            playlist_file.write(url + "\n")
        if reload:
            index = len(subprocess.run([MPC_PATH, "playlist", "playlist"], check=True,
                                       stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')) - 2
            self.reload_playlist(index)

    def remove_from_playlist(self, name):
        if self.audio_source == RADIO:
            with open("playlists/playlist.m3u", "r") as playlist_file:
                playlist_lines = playlist_file.readlines()
            print(playlist_lines)
            index = playlist_lines.index("#EXTINF:0," + name.strip() + "\n")
            playlist_lines.pop(index)
            playlist_lines.pop(index)
            with open("playlists/playlist.m3u", "w") as playlist_file:
                for line in playlist_lines:
                    playlist_file.write(line)
        elif self.audio_source == YOUTUBE:
            track_index = playlist_view_listbox.items.index(name[:28])
            subprocess.run([MPC_PATH, "del", str(int(track_index) + 1)])
            self.youtube_playlist.pop(track_index)
            playlist_out = subprocess.run([MPC_PATH, "playlist"], check=True,
                                          stdout=subprocess.PIPE).stdout.decode('utf-8')
            if playlist_out == "":
                self.start_radio()

        self.reload_playlist()

    def remove_selected_from_playlist(self):
        playlist_remove_btn.update_command(self.playlist_change_to_confirm_remove)
        print("remove_selected_from_playlist icon/clear.png")
        playlist_remove_btn.image = "icon/clear.png"
        self.remove_from_playlist(playlist_view_listbox.value)

    def playlist_change_to_confirm_remove(self):
        playlist_remove_btn.update_command(self.remove_selected_from_playlist)
        print("playlist_change_to_confirm_remove icon/done.png")
        playlist_remove_btn.image = "icon/done.png"
        self.playlist_confirm_remove_timer = threading.Timer(2.0, self.playlist_confirm_remove_reset)
        self.playlist_confirm_remove_timer.daemon = True
        self.playlist_confirm_remove_timer.start()

    def playlist_confirm_remove_reset(self):
        playlist_remove_btn.update_command(self.playlist_change_to_confirm_remove)
        print("playlist_confirm_remove_reset icon/clear.png")
        playlist_remove_btn.image = "icon/clear.png"

    def rename_track(self, old_name, new_name):
        with open("playlists/playlist.m3u", "r") as playlist_file:
            playlist_lines = playlist_file.readlines()

        index = playlist_lines.index("#EXTINF:0," + old_name + "\n")
        playlist_lines[index] = "#EXTINF:0," + new_name + "\n"

        with open("playlists/playlist.m3u", "w") as playlist_file:
            for line in playlist_lines:
                playlist_file.write(line)

        self.reload_playlist()

    def move_in_playlist(self, index, direction):
        index = int(index)
        with open("playlists/playlist.m3u", "r") as playlist_file:
            playlist_lines = playlist_file.readlines()

        # weird but it works
        index1 = (index * 2) + 1
        if direction == "UP":
            if index == 0:
                playlist_lines.insert(len(playlist_lines) - 1, playlist_lines.pop(index + 1))
                playlist_lines.insert(len(playlist_lines) - 1, playlist_lines.pop(index + 1))
                index = ((len(playlist_lines) - 1) / 2) - 1
            else:
                new_index = index1 - 2
                playlist_lines.insert(new_index, playlist_lines.pop(index1))
                playlist_lines.insert(new_index+1, playlist_lines.pop(index1+1))
                index -= 1
        else:
            if index + 1 == (len(playlist_lines) - 1) / 2:
                playlist_lines.insert(1, playlist_lines.pop(len(playlist_lines) - 1))
                playlist_lines.insert(1, playlist_lines.pop(len(playlist_lines) - 1))
                index = 0
            else:
                new_index = index1 + 3
                playlist_lines.insert(new_index, playlist_lines.pop(index1))
                playlist_lines.insert(new_index, playlist_lines.pop(index1))
                index += 1

        with open("playlists/playlist.m3u", "w") as playlist_file:
            for line in playlist_lines:
                playlist_file.write(line)

        self.reload_playlist(index)

    def move_track_up(self):
        self.move_in_playlist(playlist_view_listbox.items.index(playlist_view_listbox.value), "UP")

    def move_track_down(self):
        self.move_in_playlist(playlist_view_listbox.items.index(playlist_view_listbox.value), "DOWN")

    def show_track_qrcode(self):
        if self.audio_source == YOUTUBE:
            window_manager.open_window(QR_CODE, self.youtube_url)
        elif playlist_view_listbox.value is not None:
            track_url = subprocess.run([MPC_PATH, "current", "-f", "%file%"], check=True,
                                       stdout=subprocess.PIPE).stdout.decode('utf-8').strip()
            window_manager.open_window(QR_CODE, track_url)

    def youtube_play(self, video):
        audio_url = None
        for formats in video['formats']:
            if formats['ext'] == 'm4a' or formats['ext'] == "mp3":
                audio_url = formats['url']

        if audio_url is None:
            return

        if self.audio_source != YOUTUBE:
            self.start_youtube()

        subprocess.run([MPC_PATH, "add", audio_url])
        subprocess.run([MPC_PATH, "play"])

        track_data = {
            '0': video['uploader'],
            '1': "Youtube",
            '2': video['title'],
            'url': video['webpage_url']
        }

        self.youtube_playlist.append(track_data)

        self.info_reload()
        self.reload_playlist()

    def youtube_search(self, text, client=None):
        with yt_dlp.YoutubeDL(YDL_OPTIONS) as ydl:
            if text.startswith("https://"):
                ws_manager.ws.send(client, "youtube_search_results=Adding to queue...<br><br>" + self.yt_res_html)
                try:
                    result = ydl.extract_info(text, download=False)
                except:
                    ws_manager.ws.send(client, "youtube_search_results=Error")
                    return
                self.youtube_play(result)
                ws_manager.ws.send(client, "youtube_search_results=" + self.yt_res_html)
            else:
                ws_manager.ws.send(client, "youtube_search_results=Searching...")
                try:
                    results = YoutubeSearch(text, max_results=self.youtube_max_results).to_dict()
                except:
                    ws_manager.ws.send(client, "youtube_search_results=Error")
                    return

                self.yt_res_html = "<table id=\"youtube_search_results_table\">"
                for result in results:
                    self.yt_res_html += "<tr class=\"yt_result\" data-url=\"https://youtube.com/watch?v=" + result['id'] + "\">"

                    self.yt_res_html += "<td><img src=\"https://i.ytimg.com/vi/" + result['id'] + "/mqdefault.jpg\"></img></td>"
                    self.yt_res_html += "<td><strong>" \
                                   + result['title'] \
                                   + "</strong><br><small>" \
                                   + result['channel'] \
                                   + "</small><br><small>" \
                                   + str(result['duration']) \
                                   + "</small></td>"

                    self.yt_res_html += "</tr>\n"

                    # print(result['title'], result['channel'], result['duration'])

                self.yt_res_html += "</table>"

                ws_manager.ws.send(client, "youtube_search_results=" + self.yt_res_html)
                # ws_manager.send_to_all("youtube_search_results=" + self.yt_res_html)

    def scan_bt(self):
        bt_devices_listbox.clear()
        bluetooth_scan_status_txt.value = "Scanning..."
        bluetooth_connect_btn.image = "icon/bton.png"
        bt_scan_out = subprocess.run(["/usr/bin/expect", "scan_bt.expect"], check=True,
                                     stdout=subprocess.PIPE).stdout.decode('utf-8')

        # ansi_escape_8bit = re.compile(
        #    br'(?:\x1B[@-Z\\-_]|[\x80-\x9A\x9C-\x9F]|(?:\x1B\[|\x9B)[0-?]*[ -/]*[@-~])'
        # )
        #
        # bt_scan_out = ansi_escape_8bit.sub(b'', bt_scan_out.encode("ascii"))\
        #    .decode("ascii")\
        #    .replace("\x01", "").replace("\x02", "")

        bt_scan_out = re.split('\r\n|\n|\r', bt_scan_out)

        self.bt_devices = {}
        r = re.compile(r'^Device (([0-9A-F]{2}:){5})[0-9A-F]{2}.*$')
        for device in bt_scan_out:
            match = r.match(device)
            if match:
                splited = match.group().split(" ", maxsplit=2)
                self.bt_devices[splited[2]] = splited[1]
                bt_devices_listbox.append(splited[2])

        bluetooth_scan_status_txt.value = ""

    def scan_bt_async(self):
        threading.Thread(target=self.scan_bt, daemon=True).start()

    def choose_bt_device(self, device_name):
        self.bt_speaker_name = device_name
        self.bt_speaker_mac = self.bt_devices[device_name]
        bluetooth_connect_btn.update_command(self.select_bt_device_async)

    def select_bt_device(self):
        self.set_bt_connecting_to_speaker(True)
        # self.bt_speaker_name = device_name
        # self.bt_speaker_mac = self.bt_devices[device_name]
        bluetooth_scan_status_txt.value = "Connecting to " + self.bt_speaker_name
        hciout = subprocess.run(["/usr/bin/hcitool", "con"], check=True,
                                stdout=subprocess.PIPE).stdout.decode('utf-8')
        if self.bt_devices[self.bt_speaker_name] not in hciout:
            subprocess.run(["/usr/bin/expect", "pair_trust_connect.expect", self.bt_devices[self.bt_speaker_name]])

        subprocess.run(["/usr/bin/sudo",
                        "sed",
                        "-i",
                        "s/device.*/device " + self.bt_devices[self.bt_speaker_name] + "/",
                        "/etc/asound.conf"])
        subprocess.run(["/usr/bin/sudo", "systemctl", "restart", "mpd"])
        subprocess.run([MPC_PATH, "enable", "bluealsa"])
        subprocess.run([MPC_PATH, "disable", "equal"])
        bluetooth_scan_status_txt.value = "Connected to " + self.bt_speaker_name
        bluetooth_connect_btn.image = "icon/btcon.png"
        bluetooth_connect_btn.update_command(self.disconnect_bt_speaker_async)
        if self.audio_source == BLUETOOTH:
            subprocess.run(["/usr/bin/pkill", "-f", "bluealsa-aplay"])
            threading.Thread(target=subprocess.run, name="bluealsa-aplay",
                             args=[["/usr/bin/bluealsa-aplay",
                                    "-d",
                                    "equal" if self.bt_speaker_mac == "" else "bt-out",
                                    "00:00:00:00:00:00"]], daemon=True).start()
        threading.Timer(5, self.set_bt_connecting_to_speaker, args=[False]).start()

    def select_bt_device_async(self):
        threading.Thread(target=self.select_bt_device, daemon=True).start()

    def set_bt_connecting_to_speaker(self, value):
        self.bt_connecting_to_speaker = value

    def disconnected_bt_speaker(self, is_playing):
        # is_playing = bool(self.is_playing)
        self.bt_devices = {}
        self.bt_speaker_name = ""
        self.bt_speaker_mac = ""
        bt_devices_listbox.clear()
        subprocess.run([MPC_PATH, "enable", "equal"])
        subprocess.run([MPC_PATH, "disable", "bluealsa"])
        bluetooth_connect_btn.update_command(self.scan_bt_async)
        bluetooth_scan_status_txt.value = ""
        bluetooth_connect_btn.image = "icon/btoff.png"
        if self.audio_source == BLUETOOTH:
            subprocess.run(["/usr/bin/pkill", "-f", "bluealsa-aplay"])
            threading.Thread(target=subprocess.run, name="bluealsa-aplay",
                             args=[["/usr/bin/bluealsa-aplay",
                                    "-d",
                                    "equal" if self.bt_speaker_mac == "" else "bt-out",
                                    "00:00:00:00:00:00"]], daemon=True).start()
        if self.audio_source != BLUETOOTH:
            if is_playing:
                subprocess.run([MPC_PATH, "play"])
            else:
                subprocess.run([MPC_PATH, "stop"])

    def disconnect_bt_speaker(self):
        is_playing = bool(self.is_playing)
        self.manual_disconnect = True
        bluetooth_scan_status_txt.value = "Disconnecting from " + self.bt_speaker_name
        subprocess.run(["/usr/bin/bluetoothctl", "disconnect", self.bt_speaker_mac])
        self.disconnected_bt_speaker(is_playing)
        self.manual_disconnect = False

    def disconnect_bt_speaker_async(self):
        threading.Thread(target=self.disconnect_bt_speaker, daemon=True).start()

    def list_usb_files(self):
        if self.audio_source != USB:
            window_manager.close_window(USB_VIDEO)
            return
        #command = 'find /mnt -type f | grep -E "\.webm$|\.flv$|\.vob$|\.ogg$|\.ogv$|\.drc$|\.gifv$|\.mng$|\.avi$|\.mov$|\.qt$|\.wmv$|\.yuv$|\.rm$|\.rmvb$|/.asf$|\.amv$|\.mp4$|\.m4v$|\.mp4$|\.m?v$|\.svi$|\.3gp$|\.flv$|\.f4v$"'
        command = 'find /mnt -type f | grep -E "\\.mp4$|\\.jpg$|\\.jpeg$|\\.png$"'
        video_paths = subprocess.run(command,
                                     check=True,
                                     shell=True,
                                     stdout=subprocess.PIPE).stdout.decode('utf-8').strip().split("\n")

        usb_video_listbox.clear()
        for video in video_paths:
            usb_video_listbox.append(video)

    def play_usb_file(self):
        if usb_video_status_txt.value == "Loading..." or usb_video_listbox.value == None:
            return
        usb_video_status_txt.value = "Loading..."
        if usb_video_listbox.value.endswith(".mp4"):
            subprocess.run(['sh', 'startmpv.sh', usb_video_listbox.value])
        elif usb_video_listbox.value.endswith((".png", ".jpg", ".jpeg")):
            window_manager.open_window(IMAGE_VIEW, usb_video_listbox.value)
        usb_video_status_txt.value = ""

    def play_usb_file_async(self):
        threading.Thread(target=self.play_usb_file, daemon=True).start()

    def radio_browser_next(self, force=False):
        if self.radio_browser is None:
            try:
                self.radio_browser = RadioBrowser()
            except:
                return
        selected = radio_browser_listbox.value
        show_results = False
        reverse = True
        if selected is None and not force:
            return
        radio_browser_play_btn.hide()
        radio_browser_add_btn.hide()
        radio_browser_next_btn.show()
        radio_browser_prev_btn.show()
        radio_browser_sort_btn.hide()
        print(self.radio_browser_state)

        if self.radio_browser_state[-1] == RBS_FIRST:
            self.radio_browser_first = selected
            radio_browser_sort_btn.show()
            try:
                if self.radio_browser_first == RADIO_BROWSER_FIRST[0]:
                    values = sorted(self.radio_browser.countries(), key=lambda s: s['name'])
                    self.radio_browser_first_values_sort_by_name = True
                elif self.radio_browser_first == RADIO_BROWSER_FIRST[1]:
                    values = sorted(self.radio_browser.tags(), key=lambda s: s['stationcount'], reverse=True)
                    self.radio_browser_first_values_sort_by_name = False
            except:
                return

            self.radio_browser_first_values = values
            radio_browser_listbox.clear()
            for value in self.radio_browser_first_values:
                radio_browser_listbox.append(value['name'])
            self.radio_browser_state.append(RBS_FIRST_CHOOSING)

        elif self.radio_browser_state[-1] == RBS_FIRST_CHOOSING:
            self.radio_browser_first_value = selected
            index = radio_browser_listbox.items.index(selected)
            if self.radio_browser_first_values[index]['stationcount'] == 1:
                show_results = True
            else:
                radio_browser_listbox.clear()
                for item in RADIO_BROWSER_SORT:
                    radio_browser_listbox.append(item)
                self.radio_browser_state.append(RBS_SORT)

        elif self.radio_browser_state[-1] == RBS_SORT:
            if selected == RADIO_BROWSER_SORT[0]:
                self.radio_browser_sort_value = "clickcount"
            elif selected == RADIO_BROWSER_SORT[1]:
                self.radio_browser_sort_value = "votes"
            elif selected == RADIO_BROWSER_SORT[2]:
                self.radio_browser_sort_value = "name"
                reverse = False
            show_results = True

        if show_results:
            try:
                if self.radio_browser_first == RADIO_BROWSER_FIRST[0]:
                    self.radio_browser_results = self.radio_browser.stations_by_country(self.radio_browser_first_value,
                                                                                        order=self.radio_browser_sort_value,
                                                                                        reverse=reverse)
                elif self.radio_browser_first == RADIO_BROWSER_FIRST[1]:
                    self.radio_browser_results = self.radio_browser.stations_by_tag(self.radio_browser_first_value,
                                                                                    order=self.radio_browser_sort_value,
                                                                                    reverse=reverse)
            except:
                return
            radio_browser_listbox.clear()
            for station in self.radio_browser_results:
                radio_browser_listbox.append(station['name'])
            radio_browser_next_btn.hide()
            radio_browser_play_btn.show()
            radio_browser_add_btn.show()
            self.radio_browser_state.append(RBS_RESULTS)

        print(self.radio_browser_state)

    def radio_browser_prev(self):
        print(self.radio_browser_state)
        print("POP")
        self.radio_browser_state.pop()
        print(self.radio_browser_state)
        radio_browser_sort_btn.hide()
        radio_browser_play_btn.hide()
        radio_browser_add_btn.hide()
        radio_browser_next_btn.show()

        if self.radio_browser_state[-1] == RBS_FIRST:
            window_manager.load_radio_browser()

        elif self.radio_browser_state[-1] == RBS_FIRST_CHOOSING:
            # try:
            #     if self.radio_browser_first == RADIO_BROWSER_FIRST[0]:
            #         values = rb.countries()
            #         values = sorted(values, key=lambda s: s['name'])
            #     elif self.radio_browser_first == RADIO_BROWSER_FIRST[1]:
            #         values = rb.tags()
            # except:
            #     return
            radio_browser_sort_btn.show()
            radio_browser_listbox.clear()
            for value in self.radio_browser_first_values:
                radio_browser_listbox.append(value['name'])

            index = radio_browser_listbox.items.index(self.radio_browser_first_value)
            radio_browser_listbox.value = radio_browser_listbox.items[index]
            radio_browser_listbox.children[0].tk.see(index)

        elif self.radio_browser_state[-1] == RBS_SORT:
            radio_browser_listbox.clear()
            for item in RADIO_BROWSER_SORT:
                radio_browser_listbox.append(item)

    def radio_browser_change_sort(self):
        if self.radio_browser_first_values_sort_by_name:
            self.radio_browser_first_values = sorted(self.radio_browser_first_values, key=lambda s: s['stationcount'], reverse=True)
        else:
            self.radio_browser_first_values = sorted(self.radio_browser_first_values, key=lambda s: s['name'])
        radio_browser_listbox.clear()
        for value in self.radio_browser_first_values:
            radio_browser_listbox.append(value['name'])
        self.radio_browser_first_values_sort_by_name = not self.radio_browser_first_values_sort_by_name

    def radio_browser_play(self):
        if self.radio_browser is None:
            try:
                self.radio_browser = RadioBrowser()
            except:
                return
        # selected = radio_browser_listbox.value
        # station_index = radio_browser_listbox.items.index(selected)
        station_index = radio_browser_listbox.children[0].tk.curselection()[0]
        station = self.radio_browser_results[station_index]
        try:
            self.radio_browser.click_counter(station['stationuuid'])
        except:
            pass
        subprocess.run([MPC_PATH, "clear"])
        subprocess.run([MPC_PATH, "add", station['url']])
        subprocess.run([MPC_PATH, "play"])

    def radio_browser_add_to_playlist(self):
        station_index = radio_browser_listbox.children[0].tk.curselection()[0]
        station = self.radio_browser_results[station_index]
        self.add_to_playlist(station['name'], station['url'])


class WeatherManager:
    def __init__(self):
        self.api_key = config.config['owm_api_key']
        self.city = config.config['owm_city']
        self.lang = config.config['owm_lang']
        self.weather_reload_timer = None
        self.weather_data = {}
        self.forecast_data = {}
        self.forecast_number = 0

    def weather_data_update(self):
        url = 'https://api.openweathermap.org/data/2.5/weather?q={}&appid={}&units=metric&lang={}'.format(
            self.city,
            self.api_key,
            self.lang)
        try:
            res = requests.get(url)
        except:
            self.weather_data = None
            window_manager.close_window(WEATHER)
            return
        self.weather_data = res.json()

    def weather_reload(self):
        if self.city is None:
            return
        self.weather_data_update()
        if self.weather_data is None:
            return
        data = self.weather_data

        if int(data.get('cod')) == 200:
            pocasie_icon_img.image = "icon/" + str(data.get('weather')[0].get('icon')) + "@4x.png"

            datenow = datetime.datetime.fromtimestamp(int(data.get('dt'))).strftime('%Y-%m-%d %H:%M:%S')

            pocasie_mesto_txt.value = str(data.get('name'))
            pocasie_teplota_data_txt.value = str(round(data.get('main').get('temp'), 1)) + " °C"
            pocasie_stav_txt.value = str(data.get('weather')[0].get('description'))
            pocasie_vietor_data_txt.value = str(data.get('wind').get('speed')) + " m/s"
            pocasie_oblaky_data_txt.value = str(data.get('clouds').get('all')) + " %"
            pocasie_tlak_data_txt.value = str(data.get('main').get('pressure')) + " hPa"
            pocasie_vlhkost_data_txt.value = str(data.get('main').get('humidity')) + " %"
            pocasie_date_txt.value = str(datenow)

        elif int(data.get('cod')) == 401:
            pocasie_icon_img.image = "icon/nic.png"
            pocasie_date_txt.value = "Bad API key"

        elif int(data.get('cod')) == 404:
            pocasie_icon_img.image = "icon/nic.png"
            pocasie_date_txt.value = "Bad city"

        elif int(data.get('cod')) == 429:
            pocasie_icon_img.image = "icon/nic.png"
            pocasie_date_txt.value = "API call limit exceeded"

    def weather_reload_short(self):
        try:
            self.weather_reload_timer.cancel()
        except:
            pass
        self.weather_data_update()
        if self.weather_data is None:
            temperature_btn.text = " "
            return
        if int(self.weather_data.get('cod')) == 200:
            value = str(round(self.weather_data.get('main').get('temp'))) + " °C"
            temperature_btn.text = value
        else:
            temperature_btn.text = " "
        self.weather_reload_timer = threading.Timer(300.0, self.weather_reload_short)
        self.weather_reload_timer.daemon = True
        self.weather_reload_timer.start()

    def forecast_data_update(self):
        url = 'https://api.openweathermap.org/data/2.5/forecast?q={}&appid={}&units=metric&lang={}'.format(
            self.city,
            self.api_key,
            self.lang)
        try:
            res = requests.get(url)
        except:
            self.forecast_data = None
            window_manager.close_window(WEATHER)
            return
        self.forecast_data = res.json()
        url = 'https://api.openweathermap.org/data/2.5/weather?q={}&appid={}&units=metric&lang={}'.format(
            self.city,
            self.api_key,
            self.lang)
        try:
            res = requests.get(url)
        except:
            self.forecast_data = None
            window_manager.close_window(WEATHER)
            return
        self.forecast_data['list'].insert(0, res.json())
        self.forecast_number = 0

    def forecast_reload(self):
        if self.forecast_data is None:
            return
        forecast_data = self.forecast_data
        if self.forecast_number < 0:
            self.forecast_number = len(self.forecast_data['list']) - 1
        if self.forecast_number > len(self.forecast_data['list']) - 1:
            self.forecast_number = 0

        if int(forecast_data.get('cod')) == 200:
            pocasie_icon_img.image = "icon/" + str(
                forecast_data.get('list')[self.forecast_number].get('weather')[0].get('icon')) + "@4x.png"

            pocasie_mesto_txt.value = str(forecast_data.get('city').get('name'))
            pocasie_teplota_data_txt.value = str(
                round(forecast_data.get('list')[self.forecast_number].get('main').get('temp'), 1)) + " °C"
            pocasie_stav_txt.value = str(
                forecast_data.get('list')[self.forecast_number].get('weather')[0].get('description'))

            pocasie_vietor_data_txt.value = str(
                forecast_data.get('list')[self.forecast_number].get('wind').get('speed')) + " m/s"
            pocasie_oblaky_data_txt.value = str(
                forecast_data.get('list')[self.forecast_number].get('clouds').get('all')) + " %"
            pocasie_tlak_data_txt.value = str(
                forecast_data.get('list')[self.forecast_number].get('main').get('pressure')) + " hPa"
            pocasie_vlhkost_data_txt.value = str(
                forecast_data.get('list')[self.forecast_number].get('main').get('humidity')) + " %"
            fore_date_txt.value = str(
                datetime.datetime.fromtimestamp(
                    int(forecast_data.get('list')[self.forecast_number].get('dt'))
                ).strftime('%Y-%m-%d %H:%M:%S')
            )

        elif int(forecast_data.get('cod')) == 401:
            pocasie_icon_img.image = "icon/nic.png"
            fore_date_txt.value = "Bad API key"

        elif int(forecast_data.get('cod')) == 404:
            pocasie_icon_img.image = "icon/nic.png"
            fore_date_txt.value = "Bad city"

        elif int(forecast_data.get('cod')) == 429:
            pocasie_icon_img.image = "icon/nic.png"
            fore_date_txt.value = "API call limit exceeded"

    def switch_forecast(self, value):
        self.forecast_number += value
        self.forecast_reload()


class WebSocketServerManager:
    def __init__(self, port):
        self.ws = WebSocketServer(
            "",
            port,
            on_data_receive=self.on_data_receive,
            on_connection_open=self.on_connection_open,
            on_error=self.on_error,
            on_connection_close=self.on_connection_close,
            on_server_destruct=self.on_server_destruct
        )

        self.ws_clients = []

    def run(self):
        self.ws.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        threading.Thread(target=self.ws.serve_forever, name="websocket_server.run", daemon=True).start()

    def send_data_to_all(self, data, value):
        send_this = {
            "type": "data",
            "data": data,
            "value": value
        }
        self.send_to_all(json.dumps(send_this))

    def send_to_all(self, data):
        for ws_client in self.ws_clients:
            try:
                self.ws.send(ws_client, data)
            except BrokenPipeError:
                self.ws_clients.remove(ws_client)
                continue

    def on_data_receive(self, client, data):
        if data.startswith("{"):
            data = json.loads(data)
            if data['type'] == 'command':
                if data['command_type'] == 'player':
                    if data['command'] == 'seek':
                        player_manager.seek(data['value'])
                    elif data['command'] == 'volume':
                        player_manager.volume_control("", data['value'])
                    elif data['command'] == "volumeup":
                        player_manager.volume_control("+")
                    elif data['command'] == "volumedown":
                        player_manager.volume_control("-")
                    elif data['command'] == "track_playstop":
                        player_manager.play_stop()
                    elif data['command'] == "track_next":
                        player_manager.next_track()
                    elif data['command'] == "track_prev":
                        player_manager.prev_track()

                if data['command_type'] == 'playlist':
                    if data['command'] == 'track_play':
                        player_manager.play_track_by_name(data['value'])
                    elif data['command'] == 'track_move':
                        player_manager.move_in_playlist(data['value']['id'], data['value']['direction'])
                    elif data['command'] == 'track_rename':
                        player_manager.rename_track(data['value']['old'], data['value']['new'])
                    elif data['command'] == 'track_remove':
                        player_manager.remove_from_playlist(data['value'])
                    elif data['command'] == 'track_add':
                        player_manager.add_to_playlist(data['value']['name'], data['value']['url'])

                if data['command_type'] == 'rss':
                    if data['command'] == "rss_add":
                        config.add_rss_link(data['value']['url'])
                    elif data['command'] == "rss_remove":
                        config.remove_rss_link(data['value']['name'])
                    elif data['command'] == "rss_move":
                        config.move_rss_link(data['value']['id'], data['value']['direction'])

        elif data == "equalizersave":
            equalizer_manager.equalizer_save()
            response = "".join(
                "eq_" + str(eq) + "=" + str(equalizer_manager.get_mixer_value(EQUAL_CONTROLS[eq])) + ";"
                for eq in range(10))
            self.send_to_all(response)
        elif data == "equalizerload":
            equalizer_manager.equalizer_load()
            response = "".join(
                "eq_" + str(eq) + "=" + str(equalizer_manager.get_mixer_value(EQUAL_CONTROLS[eq])) + ";"
                for eq in range(10))
            self.send_to_all(response)

        elif data.startswith("eq_"):
            split = data.split("=")
            mixer = int(re.search(r'\d+', split[0]).group())
            value = int(split[1])
            equalizer_manager.set_mixer_value(value, EQUAL_CONTROLS[mixer])

        elif data.startswith("reload_playlist"):
            player_manager.reload_playlist()
            # self.send_to_all("playlist_data=" + player_manager.get_playlist_json())

        elif player_manager.audio_source != BLUETOOTH:
            if data.startswith("youtube"):
                player_manager.youtube_search(data[8:], client)

        self.ws.send(client, "OK")

    def on_connection_open(self, client):
        # self.ws.send(client, "Welcome to the server!")
        response = ("volume=" + str(player_manager.get_volume_value())
                    + ";info1=" + player_manager.info_lines[0]
                    + ";info2=" + player_manager.info_lines[1]
                    + ";info3=" + player_manager.info_lines[2])
        for eq in range(10):
            response += ";eq_" + str(eq) + "=" + str(equalizer_manager.get_mixer_value(EQUAL_CONTROLS[eq]))
        self.ws.send(client, response)
        self.ws.send(client, player_manager.get_playlist_json())
        self.ws.send(client, rss_manager.get_rss_links_json())
        self.ws_clients.append(client)

    def on_error(self, exception):
        raise exception

    def on_connection_close(self, client):
        self.ws_clients.remove(client)

    def on_server_destruct(self):
        pass


class BatteryManager:
    def __init__(self):
        self.MIN_VOLTAGE = 16
        self.MAX_VOLTAGE = 21
        self.VOLTAGE_DIV = self.MAX_VOLTAGE - self.MIN_VOLTAGE

        self.update_timer = None

        self.current_voltage = ina226.busVoltage()
        self.current_percentage = min(round(((self.current_voltage - self.MIN_VOLTAGE) / self.VOLTAGE_DIV) * 100), 100)

        # self.current_voltage_averages = [self.current_voltage] * 10
        # self.current_voltage_averages = []
        # self.current_voltage_average = self.current_voltage
        # self.previous_voltage_average = self.current_voltage

        self.is_charging = False

    def measure(self):
        prev_voltage = self.current_voltage
        self.current_voltage = ina226.busVoltage()
        self.current_percentage = min(round(((self.current_voltage - self.MIN_VOLTAGE) / self.VOLTAGE_DIV) * 100), 100)

        if self.current_voltage - prev_voltage > 0.2:
            self.is_charging = True
        elif prev_voltage - self.current_voltage > 0.2:
            self.is_charging = False

        # self.current_voltage_averages.pop(0)
        # self.current_voltage_averages.append(self.current_voltage)
        # elif len(self.current_voltage_averages) >= 10:
        #     self.current_voltage_average = sum(self.current_voltage_averages) / len(self.current_voltage_averages)
        #     if self.current_voltage_average - self.previous_voltage_average > 0.02:
        #         self.is_charging = True
        #     else:
        #         self.is_charging = False
        #     self.previous_voltage_average = self.current_voltage_average
        #     self.current_voltage_averages.clear()

        # print(str(self.current_percentage) + "% (" + str(round(self.current_voltage, 2)) + " V)")
        # print(self.current_voltage, self.current_voltage_average, self.is_charging)

        self.update_timer = threading.Timer(1.0, self.measure)
        self.update_timer.daemon = True
        self.update_timer.start()

    def update_status(self):
        battery_status_txt.value = str(self.current_percentage) + "%"
        if self.is_charging:
            battery_status_img.image = "icon/battery_charging.png"
        else:
            percent_value = int(self.current_percentage / (100 / 7))
            battery_status_img.image = f"icon/battery_{percent_value}.png"


def async_call(function):
    threading.Thread(target=function, daemon=True).start()


def mpc_idleloop():
    process = subprocess.Popen([MPC_PATH, "idleloop"], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    while process.stdout.readable():
        line = process.stdout.readline().decode("utf-8").strip()
        if line == "MPD error: Connection closed by the server":
            break

        if not line:
            continue

        if player_manager.audio_source != BLUETOOTH:
            if line == "player":
                player_manager.reload_playing_status()
                player_manager.info_reload()

        if line == "mixer":
            player_manager.volume_reload()

    threading.Thread(target=mpc_idleloop, name="mpc_idleloop", daemon=True).start()


def main_clock_update():
    clockdata = "%s" % time.strftime("%H:%M:%S")
    clock_btn.text = clockdata
    datedata = "%s" % time.strftime("%d.%m.%Y")
    if date_btn.text != datedata:
        date_btn.text = datedata

    if player_manager.audio_source == YOUTUBE or player_manager.audio_source == USB:
        try:
            position_cmd_out = subprocess.run([MPC_PATH], check=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
            position_txt.value = re.search(r'[0-9]+:[0-9]{2}/[0-9]+:[0-9]{2}', position_cmd_out).group()
        except AttributeError:
            position_txt.value = ""
    else:
        position_txt.value = ""


def clock_reolad():
    clocktime = "%s" % time.strftime("%H:%M:%S")
    clock_full_btn.text = clocktime


def about_reload():
    with open("/sys/class/thermal/thermal_zone0/temp", "r") as f:
        temperature = f.read(2)
    uptime_cmd_out = subprocess.run(["/usr/bin/uprecords"], check=True,
                                    stdout=subprocess.PIPE).stdout.decode('utf-8').split('\n')
    uptime = list(filter(re.compile("^ {4}up.*\|").match, uptime_cmd_out))[0].split('up')[1].split('|')[0]
    uptime = uptime.strip()
    ipadr = subprocess.run(["/bin/hostname", "-I"], check=True, stdout=subprocess.PIPE).stdout.decode('utf-8')[:-1]
    voltage = ina226.busVoltage()
    # timedata = "%s" %time.strftime("%d.%m.%y    %H:%M:%S")
    settings_info_temperature_text.value = f"{temperature} °C"
    settings_info_ipaddress_text.value = ipadr
    settings_info_total_uptime_text.value = uptime
    settings_info_total_battery_voltage_text.value = str(round(((voltage - 16) / (21 - 16)) * 100)) + "% (" + str(round(voltage, 2)) + " V)"


def exit_app():
    app.destroy()


def power_restart():
    exit_app()


def power_shutdown():
    subprocess.run([MPC_PATH, "stop"])
    # exit_app()
    subprocess.run(["/usr/bin/sudo", "shutdown", "now"])


def stop_radio_service():
    subprocess.run(["/usr/bin/sudo", "/bin/systemctl", "stop", "radio"])


def all_children(wid):
    _list = wid.children

    for item in _list:
        try:
            if item.children:
                _list.extend(item.children)
        except AttributeError:
            pass

    return _list


def set_default_style():
    loaded_img.image = "loaded.png"
    for el in all_children(app):
        if type(el) == PushButton:
            el.padding(0, 0)
            el.tk.config(relief=tk.FLAT, highlightthickness=0)
            el.font = config.config['font']
        elif type(el) == Window:
            el.height = 320
            el.width = 480
        elif type(el) == Text:
            el.font = config.config['font']
        elif type(el) == ListBox:
            el.font = config.config['font']

    loading_window.hide()


def open_vizualizer():
    subprocess.run("xterm cava -geometry 79x24 -b 0 -bw 4 -bg black -display :0", shell=True)


ina226 = INA226.INA226()

app = App(height=320, width=480, bg="black")

loading_window = Window(app, bg="black", height=320, width=480)
loaded_img = Picture(loading_window, "splash/1.png", height=320, width=480)
loaded_img.when_clicked = power_restart

try:
    rb = RadioBrowser()
except:
    rb = None

config = Config("config.yaml")
server_files = ServerFiles()
threading.Thread(target=server_files.load_files, name="server_files.load_files", daemon=True).start()
window_manager = WindowManager([MAIN])
weather_manager = WeatherManager()
rss_manager = RssManager()
player_manager = PlayerManager(rb, RADIO, True, ["", "", ""], config.config['volume_step'], config.config['yt_max_results'])
ws_manager = WebSocketServerManager(1337)
battery_manager = BatteryManager()

########################################
# Startup notice -----------------------
########################################

notice_window = Window(app, bg="black", title=NOTICE, visible=False, width=480, height=320)
notice_window.hide()

notice_error_txt = Text(notice_window, text=config.error_message, align="top", color="white")
notice_close_btn = PushButton(notice_window, image="icon/done.png", align="bottom", command=notice_window.hide)

########################################
# Menu ---------------------------------
########################################

menu_window = Window(app, bg="black", title=MENU, visible=False)
menu_window.hide()

window_manager.add_window(menu_window)

menu_top_box = Box(menu_window, align="top", width=480, height=40)
menu_bottom_box = Box(menu_window, align="bottom", width=480, height=80)
menu_middle_box = Box(menu_window, layout="grid", align="top")

open_weather_btn = PushButton(menu_middle_box, image="icon/cloud.png", grid=[0, 0],
                              command=window_manager.open_window, args=[WEATHER, 0],
                              align="left")
open_rss_menu_btn = PushButton(menu_middle_box, image="icon/rss.png", grid=[1, 0],
                               command=window_manager.open_window, args=[RSS_MENU],
                               align="left")
open_equalizer_btn = PushButton(menu_middle_box, image="icon/equalizer.png", grid=[2, 0],
                                command=window_manager.open_window, args=[EQUALIZER_LOW],
                                align="left")
open_settings_btn = PushButton(menu_middle_box, image="icon/settings.png", grid=[0, 1],
                               command=window_manager.open_window,
                               args=[SETTINGS],
                               align="left")
open_music_btn = PushButton(menu_middle_box, image="icon/mp3.png", grid=[1, 1],
                            command=window_manager.open_window, args=[AUDIO_MENU],
                            align="left")
device_qrcode_btn = PushButton(menu_middle_box, image="icon/qr_code.png", grid=[2, 1],
                               command=window_manager.show_device_ip_qrcode,
                               align="left")

# qrcode_image_picture = Picture(menu_middle_box, image="icon/info.png", width=150, height=150, align="left")

open_exit_btn = PushButton(menu_bottom_box, image="icon/power_menu.png",
                           command=window_manager.open_window, args=[POWER_MENU],
                           align="right")
close_menu_btn = PushButton(menu_bottom_box, image="icon/back.png",
                            command=window_manager.close_window, args=[MENU],
                            align="left")

battery_box = Box(menu_bottom_box, align="top")
battery_status_img = Picture(battery_box, align="left", image="icon/nic.png")
battery_status_txt = Text(battery_box, text="", color="white", size=18,
                          align="left")

########################################


########################################
# RSS menu -----------------------------
########################################

rss_menu_window = Window(app, bg="black", title=RSS_MENU, visible=False)
rss_menu_window.hide()

window_manager.add_window(rss_menu_window)

rss_links_listbox = ListBox(rss_menu_window, height=240, width=480, items=[], scrollbar=True,
                            command=rss_manager.rss_link_selected)

rss_links_listbox.text_color = "white"
rss_links_listbox.text_size = 20
rss_links_listbox.children[0].tk.config(relief=tk.FLAT, highlightthickness=0, activestyle=tk.NONE, selectmode=tk.SINGLE)
rss_links_listbox.children[1].tk.config(width=32, activebackground="#606060", bg="#808080", bd=0)

# rss_list_box = Box(rss_menu_window)
# rss_domov_btn = PushButton(rss_buttons_box, image="icon/home.png", command=window_manager.open_window,
#                            args=[RSS_VIEW, 0],
#                            align="left")
# rss_zahr_btn = PushButton(rss_buttons_box, image="icon/map.png", command=window_manager.open_window,
#                           args=[RSS_VIEW, 1],
#                           align="left")
# rss_sport_btn = PushButton(rss_buttons_box, image="icon/bike.png", command=window_manager.open_window,
#                            args=[RSS_VIEW, 2],
#                            align="left")
# rss_ekon_btn = PushButton(rss_buttons_box, image="icon/euro.png", command=window_manager.open_window,
#                           args=[RSS_VIEW, 3],
#                           align="left")

close_rss_menu_box = Box(rss_menu_window, align="bottom", width=480, height=80)
close_rss_menu_btn = PushButton(close_rss_menu_box, image="icon/back.png",
                                command=window_manager.close_window, args=[RSS_MENU], align="left")
rss_links_errors_txt = Text(close_rss_menu_box, text="", align="left", color="white")
rss_open_btn = PushButton(close_rss_menu_box, image="icon/back180_off.png", align="right",
                          command=rss_manager.open_rss)
reload_rss_links_btn = PushButton(close_rss_menu_box, image="icon/refresh.png", align="right",
                                  command=rss_manager.reload_links_async, visible=False)
move_rss_link_down_btn = PushButton(close_rss_menu_box, image="icon/down.png", align="right",
                                    command=config.move_rss_link_down, visible=False)
move_rss_link_up_btn = PushButton(close_rss_menu_box, image="icon/up.png", align="right",
                                  command=config.move_rss_link_up, visible=False)
rss_edit_mode_btn = PushButton(close_rss_menu_box, image="icon/list.png", align="right",
                               command=window_manager.toggle_rss_edit_mode)

########################################


########################################
# RSS ----------------------------------
########################################

rss_window = Window(app, bg="black", title=RSS_VIEW, visible=False)
rss_window.hide()

window_manager.add_window(rss_window)

rss_text_box = Box(rss_window, layout="grid", align="top", width=480, height=240)

rss_title_txt = Text(rss_text_box, size=16, color="#7CB342", grid=[0, 0], text="Ereses", align="left")
rss_title_txt.when_clicked = lambda: rss_manager.show_rss_qrcode()

rss_description_txt = Text(rss_text_box, size=12, color="white", grid=[0, 1], text="Ereses", align="top")

rss_title_txt.tk.config(wraplength=480, anchor=tk.W, justify=tk.LEFT)
rss_description_txt.tk.config(wraplength=470, justify=tk.LEFT)

close_rss_box = Box(rss_window, layout="auto", align="bottom", width=480, height=80)

close_rss_btn = PushButton(close_rss_box, image="icon/back.png",
                           command=window_manager.close_window, args=[RSS_VIEW],
                           align="left")
rss_date_txt = Text(close_rss_box, size=16, color="white", text="Date", align="left")
rss_next_btn = PushButton(close_rss_box, image="icon/back180.png",
                          command=rss_manager.next_rss,
                          align="right")
rss_prev_btn = PushButton(close_rss_box, image="icon/back.png",
                          command=rss_manager.prev_rss,
                          align="right")

########################################


########################################
# Clock --------------------------------
########################################

clock_window = Window(app, bg="black", title=CLOCK, visible=False)
clock_window.hide()

window_manager.add_window(clock_window)

clock_box = Box(clock_window, height=320, width=480)

clock_full_btn = PushButton(clock_box, text="Time", align="left", height=320, width=480,
                            command=window_manager.close_window, args=[CLOCK])
clock_full_btn.text_size = 70
clock_full_btn.text_color = "white"
clock_full_btn.font = "monospace"

########################################


########################################
# Calendar -----------------------------
########################################

calendar_window = Window(app, bg="black", title=CALENDAR, visible=False)
calendar_window.hide()

window_manager.add_window(calendar_window)

calendar_padding_box = Box(calendar_window, align="top", height=12, width=480)
calendar_main_box = Box(calendar_window, align="top", height=228, width=480)
calendar_txt = Text(calendar_main_box, align="top", size=20, color="white")

calendar_txt.tk.config(justify=tk.LEFT)

calendar_bottom_box = Box(calendar_window, align="bottom", height=80, width=480)
calendar_close_btn = PushButton(calendar_bottom_box, align="left", image="icon/back.png",
                                command=window_manager.close_window, args=[CALENDAR])
calendar_month_txt = Text(calendar_bottom_box, align="left", size=20, color="white")
calendar_next_btn = PushButton(calendar_bottom_box, align="right", image="icon/back180.png",
                               command=window_manager.calendar_change_diff, args=[1])
calendar_prev_btn = PushButton(calendar_bottom_box, align="right", image="icon/back.png",
                               command=window_manager.calendar_change_diff, args=[-1])

########################################


########################################
# Power menu ---------------------------
########################################

power_menu_window = Window(app, bg="black", title=POWER_MENU, visible=False)
power_menu_window.hide()

window_manager.add_window(power_menu_window)

power_menu_close_top_box = Box(power_menu_window, align="top", width=480, height=120)
power_menu_close_top_btn = PushButton(power_menu_close_top_box, image="icon/nic.png", height=120, width=480,
                                      command=window_manager.close_window, args=[POWER_MENU],
                                      align="top")

power_menu_close_middle_box = Box(power_menu_window, align="top", width=480, height=80)
power_menu_close_middle_space0_btn = PushButton(power_menu_close_middle_box, image="icon/nic.png", height=75, width=90,
                                                command=window_manager.close_window, args=[POWER_MENU],
                                                align="left")
no_exit_btn = PushButton(power_menu_close_middle_box, image="icon/restart.png",
                         command=window_manager.open_window, args=[RESTART],
                         align="left")
power_menu_close_middle_space1_btn = PushButton(power_menu_close_middle_box, image="icon/nic.png", height=75, width=125,
                                                command=window_manager.close_window, args=[POWER_MENU],
                                                align="left")

power_menu_close_middle_space2_btn = PushButton(power_menu_close_middle_box, image="icon/nic.png", height=75, width=90,
                                                command=window_manager.close_window, args=[POWER_MENU],
                                                align="right")
yes_exit_btn = PushButton(power_menu_close_middle_box, image="icon/power.png",
                          command=window_manager.open_window, args=[SHUTDOWN],
                          align="right")

power_menu_close_bottom_box = Box(power_menu_window, align="bottom", width=480, height=120)
power_menu_close_bottom_btn = PushButton(power_menu_close_bottom_box, image="icon/nic.png", height=120, width=480,
                                         command=window_manager.close_window, args=[POWER_MENU],
                                         align="bottom")

########################################

power_menu_confirm_window = Window(app, bg="black", title=POWER_MENU_CONFIRM, visible=False)
power_menu_confirm_window.hide()

window_manager.add_window(power_menu_confirm_window, RESTART)
window_manager.add_window(power_menu_confirm_window, SHUTDOWN)

power_menu_confirm_top_box = Box(power_menu_confirm_window, align="top", width=480, height=120)

power_menu_confirm_top_btn = PushButton(power_menu_confirm_top_box, image="icon/nic.png", height=120, width=480,
                                        command=window_manager.close_window, args=[POWER_MENU_CONFIRM],
                                        align="top")

power_menu_confirm_middle_box = Box(power_menu_confirm_window, align="top", width=480, height=80)
power_menu_confirm_middle_space0_btn = PushButton(power_menu_confirm_middle_box, image="icon/nic.png", height=75,
                                                  width=193,
                                                  command=window_manager.close_window, args=[POWER_MENU_CONFIRM],
                                                  align="left")
power_menu_confirm_btn = PushButton(power_menu_confirm_middle_box, image="icon/power.png",
                                    command=exit_app,
                                    align="left")
power_menu_confirm_middle_space1_btn = PushButton(power_menu_confirm_middle_box, image="icon/nic.png", height=75,
                                                  width=193,
                                                  command=window_manager.close_window, args=[POWER_MENU_CONFIRM],
                                                  align="left")

power_menu_confirm_bottom_box = Box(power_menu_confirm_window, align="bottom", width=480, height=120)
power_menu_confirm_bottom_btn = PushButton(power_menu_confirm_bottom_box, image="icon/nic.png", height=120, width=480,
                                           command=window_manager.close_window,
                                           args=[POWER_MENU_CONFIRM],
                                           align="bottom")

########################################


########################################
# Settings lock ------------------------
########################################

settings_lock_window = Window(menu_window, bg="black", title=SETTINGS_LOCK, visible=False)
settings_lock_window.hide()

window_manager.add_window(settings_lock_window)

settings_lock_input_box = Box(settings_lock_window, width=480, height=80, align="top")
settings_lock_input_tip_txt = Text(settings_lock_input_box, text="Enter pin: ", color="white", size=16, align="left")
settings_lock_input_txt = Text(settings_lock_input_box, text="", color="white", size=16, align="left")

settings_lock_top_box = Box(settings_lock_window, align="top", width=480, height=80)
settings_lock_nt_btn = PushButton(settings_lock_top_box, image="icon/nic.png",
                                  align="left")

settings_lock_1_btn = PushButton(settings_lock_top_box, image="icon/1.png",
                                 align="left",
                                 command=lambda: window_manager.pin_add('1'))
settings_lock_2_btn = PushButton(settings_lock_top_box, image="icon/2.png",
                                 align="left",
                                 command=lambda: window_manager.pin_add('2'))
settings_lock_3_btn = PushButton(settings_lock_top_box, image="icon/3.png",
                                 align="left",
                                 command=lambda: window_manager.pin_add('3'))
settings_lock_x_btn = PushButton(settings_lock_top_box, image="icon/clear.png",
                                 align="left",
                                 command=window_manager.pin_remove)
settings_lock_otp_btn = PushButton(settings_lock_top_box, image="icon/rss.png",
                                   align="left")

settings_lock_middle_box = Box(settings_lock_window, align="top", width=480, height=80)
settings_lock_nm_btn = PushButton(settings_lock_middle_box, image="icon/nic.png",
                                  align="left")

settings_lock_4_btn = PushButton(settings_lock_middle_box, image="icon/4.png",
                                 align="left",
                                 command=lambda: window_manager.pin_add('4'))
settings_lock_5_btn = PushButton(settings_lock_middle_box, image="icon/5.png",
                                 align="left",
                                 command=lambda: window_manager.pin_add('5'))
settings_lock_6_btn = PushButton(settings_lock_middle_box, image="icon/6.png",
                                 align="left",
                                 command=lambda: window_manager.pin_add('6'))
settings_lock_ok_btn = PushButton(settings_lock_middle_box, image="icon/done.png",
                                  align="left",
                                  command=window_manager.check_lock)
settings_lock_reset_btn = PushButton(settings_lock_middle_box, image="icon/restart.png",
                                     align="left",
                                     command=lambda: window_manager.open_window(RESET_CONFIG))

settings_lock_bottom_box = Box(settings_lock_window, align="bottom", width=480, height=80)

settings_lock_close_btn = PushButton(settings_lock_bottom_box, image="icon/back.png",
                                     align="left",
                                     command=window_manager.close_window,
                                     args=[SETTINGS_LOCK])
settings_lock_7_btn = PushButton(settings_lock_bottom_box, image="icon/7.png",
                                 align="left",
                                 command=lambda: window_manager.pin_add('7'))
settings_lock_8_btn = PushButton(settings_lock_bottom_box, image="icon/8.png",
                                 align="left",
                                 command=lambda: window_manager.pin_add('8'))
settings_lock_9_btn = PushButton(settings_lock_bottom_box, image="icon/9.png",
                                 align="left",
                                 command=lambda: window_manager.pin_add('9'))
settings_lock_0_btn = PushButton(settings_lock_bottom_box, image="icon/0.png",
                                 align="left",
                                 command=lambda: window_manager.pin_add('0'))
settings_lock_info_btn = PushButton(settings_lock_bottom_box, image="icon/info.png",
                                    align="left")

########################################


########################################
# Reset config -------------------------
########################################

reset_config_window = Window(menu_window, bg="black", title=RESET_CONFIG, visible=False)
reset_config_window.hide()

window_manager.add_window(reset_config_window)

reset_config_text_box = Box(reset_config_window, align="top", width=480, height=160)
reset_config_text_btn = PushButton(reset_config_text_box, text="Reset Configuration?", width=480, height=80,
                                   command=lambda: window_manager.close_window(RESET_CONFIG))

reset_config_button_box = Box(reset_config_window, align="top", width=480, height=160)

reset_config_button_top_box = Box(reset_config_button_box, align="top", width=480, height=80)
reset_config_button_bottom_box = Box(reset_config_button_box, align="bottom", width=480, height=80)

reset_config_exit0_btn = PushButton(reset_config_button_top_box, image="icon/nic.png", height=80, width=200,
                                    command=lambda: window_manager.close_window(RESET_CONFIG),
                                    align="left")
reset_config_confirm_btn = PushButton(reset_config_button_top_box, image="icon/restart.png",
                                      command=config.reset_config,
                                      align="left")
reset_config_exit1_btn = PushButton(reset_config_button_top_box, image="icon/nic.png", height=80, width=200,
                                    command=lambda: window_manager.close_window(RESET_CONFIG),
                                    align="left")
reset_config_exit2_btn = PushButton(reset_config_button_bottom_box, image="icon/nic.png", height=80, width=480,
                                    command=lambda: window_manager.close_window(RESET_CONFIG),
                                    align="bottom")

reset_config_text_btn.text_color = "white"
reset_config_text_btn.text_size = 20

########################################


########################################
# Settings -----------------------------
########################################

settings_window = Window(menu_window, bg="black", title=SETTINGS, visible=False)
settings_window.hide()

window_manager.add_window(settings_window)

settings_box = Box(settings_window, align="top", width=480, height=240)

settings_info_box = Box(settings_box, layout="grid", align="top", width=480, height=160)
settings_info_ipaddress = Text(settings_info_box, grid=[0, 0], text="IP: ", color="white", size=16, align="right")
settings_info_temperature = Text(settings_info_box, grid=[0, 1], text="CPU: ", color="white", size=16, align="right")
settings_info_total_uptime = Text(settings_info_box, grid=[0, 2], text="Uptime: ", color="white", size=16,
                                  align="right")
settings_info_battery_voltage = Text(settings_info_box, grid=[0, 3], text="Battery: ", color="white", size=16,
                                     align="right")
settings_info_ipaddress_text = Text(settings_info_box, grid=[1, 0], text="Načítavam info...", color="white", size=18,
                                    align="left")
settings_info_temperature_text = Text(settings_info_box, grid=[1, 1], text="Načítavam info...", color="white", size=18,
                                      align="left")
settings_info_total_uptime_text = Text(settings_info_box, grid=[1, 2], text="Načítavam info...", color="white", size=18,
                                       align="left")
settings_info_total_battery_voltage_text = Text(settings_info_box, grid=[1, 3], text="Načítavam info...", color="white", size=18,
                                                align="left")
# settings_stop_service_btn = PushButton(settings_box, image="icon/power.png",
#                                        command=stop_radio_service,
#                                        align="right")

settings_bottom_box = Box(settings_window, layout="auto", align="bottom", width=480, height=80)
settings_close_btn = PushButton(settings_bottom_box, image="icon/back.png",
                                command=window_manager.close_window, args=[SETTINGS],
                                align="left")
settings_allow_read_write_btn = PushButton(settings_bottom_box, image="icon/settings_off.png",
                                           command=window_manager.check_config_lock,
                                           align="right")
settings_open_vizualizer_btn = PushButton(settings_bottom_box, image="icon/equalizer.png",
                                          command=open_vizualizer,
                                          align="right")

########################################


########################################
# Equalizer ----------------------------
########################################

equalizer_low_window = Window(menu_window, bg="black", title=EQUALIZER_LOW, visible=False)
equalizer_low_sliders_box = Box(equalizer_low_window, layout="auto", align="top", width=480, height=240)
equalizer_low_window.hide()

window_manager.add_window(equalizer_low_window)

equalizer_high_window = Window(menu_window, bg="black", title=EQUALIZER_HIGH, visible=False)
equalizer_high_sliders_box = Box(equalizer_high_window, layout="auto", align="top", width=480, height=240)
equalizer_high_window.hide()

window_manager.add_window(equalizer_high_window)

equalizer_control_boxes = [
    Box(equalizer_low_sliders_box, layout="auto", align="left", width=96, height=240),
    Box(equalizer_low_sliders_box, layout="auto", align="left", width=96, height=240),
    Box(equalizer_low_sliders_box, layout="auto", align="left", width=96, height=240),
    Box(equalizer_low_sliders_box, layout="auto", align="left", width=96, height=240),
    Box(equalizer_low_sliders_box, layout="auto", align="left", width=96, height=240),
    Box(equalizer_high_sliders_box, layout="auto", align="left", width=96, height=240),
    Box(equalizer_high_sliders_box, layout="auto", align="left", width=96, height=240),
    Box(equalizer_high_sliders_box, layout="auto", align="left", width=96, height=240),
    Box(equalizer_high_sliders_box, layout="auto", align="left", width=96, height=240),
    Box(equalizer_high_sliders_box, layout="auto", align="left", width=96, height=240)
]

equalizer_sliders = [
    Slider(equalizer_control_boxes[0], start=-100, end=0, horizontal=False, align="right", height=240, width=15,
           command=lambda x: equalizer_manager.change_mixer(x, 0)),
    Slider(equalizer_control_boxes[1], start=-100, end=0, horizontal=False, align="right", height=240, width=15,
           command=lambda x: equalizer_manager.change_mixer(x, 1)),
    Slider(equalizer_control_boxes[2], start=-100, end=0, horizontal=False, align="right", height=240, width=15,
           command=lambda x: equalizer_manager.change_mixer(x, 2)),
    Slider(equalizer_control_boxes[3], start=-100, end=0, horizontal=False, align="right", height=240, width=15,
           command=lambda x: equalizer_manager.change_mixer(x, 3)),
    Slider(equalizer_control_boxes[4], start=-100, end=0, horizontal=False, align="right", height=240, width=15,
           command=lambda x: equalizer_manager.change_mixer(x, 4)),
    Slider(equalizer_control_boxes[5], start=-100, end=0, horizontal=False, align="right", height=240, width=15,
           command=lambda x: equalizer_manager.change_mixer(x, 5)),
    Slider(equalizer_control_boxes[6], start=-100, end=0, horizontal=False, align="right", height=240, width=15,
           command=lambda x: equalizer_manager.change_mixer(x, 6)),
    Slider(equalizer_control_boxes[7], start=-100, end=0, horizontal=False, align="right", height=240, width=15,
           command=lambda x: equalizer_manager.change_mixer(x, 7)),
    Slider(equalizer_control_boxes[8], start=-100, end=0, horizontal=False, align="right", height=240, width=15,
           command=lambda x: equalizer_manager.change_mixer(x, 8)),
    Slider(equalizer_control_boxes[9], start=-100, end=0, horizontal=False, align="right", height=240, width=15,
           command=lambda x: equalizer_manager.change_mixer(x, 9))
]

equalizer_buttons_boxes = [
    Box(master=equalizer_control_boxes[0], layout="auto", align="left", width=75, height=240),
    Box(master=equalizer_control_boxes[1], layout="auto", align="left", width=75, height=240),
    Box(master=equalizer_control_boxes[2], layout="auto", align="left", width=75, height=240),
    Box(master=equalizer_control_boxes[3], layout="auto", align="left", width=75, height=240),
    Box(master=equalizer_control_boxes[4], layout="auto", align="left", width=75, height=240),
    Box(master=equalizer_control_boxes[5], layout="auto", align="left", width=75, height=240),
    Box(master=equalizer_control_boxes[6], layout="auto", align="left", width=75, height=240),
    Box(master=equalizer_control_boxes[7], layout="auto", align="left", width=75, height=240),
    Box(master=equalizer_control_boxes[8], layout="auto", align="left", width=75, height=240),
    Box(master=equalizer_control_boxes[9], layout="auto", align="left", width=75, height=240)
]

equalizer_buttons = [
    [
        PushButton(master=equalizer_buttons_boxes[0], image="icon/plus.png",
                   align="top", command=lambda: equalizer_manager.change_mixer('+', 0)),
        Text(master=equalizer_buttons_boxes[0], text="N/A", color="white", size=16),
        PushButton(master=equalizer_buttons_boxes[0], image="icon/minus.png",
                   align="bottom", command=lambda: equalizer_manager.change_mixer('-', 0)),
        Text(master=equalizer_buttons_boxes[0], text=EQUAL_CONTROLS[0][4:], color="white", size=16, align="bottom")
    ], [
        PushButton(master=equalizer_buttons_boxes[1], image="icon/plus.png",
                   align="top", command=lambda: equalizer_manager.change_mixer('+', 1)),
        Text(master=equalizer_buttons_boxes[1], text="N/A", color="white", size=16),
        PushButton(master=equalizer_buttons_boxes[1], image="icon/minus.png",
                   align="bottom", command=lambda: equalizer_manager.change_mixer('-', 1)),
        Text(master=equalizer_buttons_boxes[1], text=EQUAL_CONTROLS[1][4:], color="white", size=16, align="bottom")
    ], [
        PushButton(master=equalizer_buttons_boxes[2], image="icon/plus.png",
                   align="top", command=lambda: equalizer_manager.change_mixer('+', 2)),
        Text(master=equalizer_buttons_boxes[2], text="N/A", color="white", size=16),
        PushButton(master=equalizer_buttons_boxes[2], image="icon/minus.png",
                   align="bottom", command=lambda: equalizer_manager.change_mixer('-', 2)),
        Text(master=equalizer_buttons_boxes[2], text=EQUAL_CONTROLS[2][4:], color="white", size=16, align="bottom")
    ], [
        PushButton(master=equalizer_buttons_boxes[3], image="icon/plus.png",
                   align="top", command=lambda: equalizer_manager.change_mixer('+', 3)),
        Text(master=equalizer_buttons_boxes[3], text="N/A", color="white", size=16),
        PushButton(master=equalizer_buttons_boxes[3], image="icon/minus.png",
                   align="bottom", command=lambda: equalizer_manager.change_mixer('-', 3)),
        Text(master=equalizer_buttons_boxes[3], text=EQUAL_CONTROLS[3][4:], color="white", size=16, align="bottom")
    ], [
        PushButton(master=equalizer_buttons_boxes[4], image="icon/plus.png",
                   align="top", command=lambda: equalizer_manager.change_mixer('+', 4)),
        Text(master=equalizer_buttons_boxes[4], text="N/A", color="white", size=16),
        PushButton(master=equalizer_buttons_boxes[4], image="icon/minus.png",
                   align="bottom", command=lambda: equalizer_manager.change_mixer('-', 4)),
        Text(master=equalizer_buttons_boxes[4], text=EQUAL_CONTROLS[4][4:], color="white", size=16, align="bottom")
    ], [
        PushButton(master=equalizer_buttons_boxes[5], image="icon/plus.png",
                   align="top", command=lambda: equalizer_manager.change_mixer('+', 5)),
        Text(master=equalizer_buttons_boxes[5], text="N/A", color="white", size=16),
        PushButton(master=equalizer_buttons_boxes[5], image="icon/minus.png",
                   align="bottom", command=lambda: equalizer_manager.change_mixer('-', 5)),
        Text(master=equalizer_buttons_boxes[5], text=EQUAL_CONTROLS[5][4:], color="white", size=16, align="bottom")
    ], [
        PushButton(master=equalizer_buttons_boxes[6], image="icon/plus.png",
                   align="top", command=lambda: equalizer_manager.change_mixer('+', 6)),
        Text(master=equalizer_buttons_boxes[6], text="N/A", color="white", size=16),
        PushButton(master=equalizer_buttons_boxes[6], image="icon/minus.png",
                   align="bottom", command=lambda: equalizer_manager.change_mixer('-', 6)),
        Text(master=equalizer_buttons_boxes[6], text=EQUAL_CONTROLS[6][4:], color="white", size=16, align="bottom")
    ], [
        PushButton(master=equalizer_buttons_boxes[7], image="icon/plus.png",
                   align="top", command=lambda: equalizer_manager.change_mixer('+', 7)),
        Text(master=equalizer_buttons_boxes[7], text="N/A", color="white", size=16),
        PushButton(master=equalizer_buttons_boxes[7], image="icon/minus.png",
                   align="bottom", command=lambda: equalizer_manager.change_mixer('-', 7)),
        Text(master=equalizer_buttons_boxes[7], text=EQUAL_CONTROLS[7][4:], color="white", size=16, align="bottom")
    ], [
        PushButton(master=equalizer_buttons_boxes[8], image="icon/plus.png",
                   align="top", command=lambda: equalizer_manager.change_mixer('+', 8)),
        Text(master=equalizer_buttons_boxes[8], text="N/A", color="white", size=16),
        PushButton(master=equalizer_buttons_boxes[8], image="icon/minus.png",
                   align="bottom", command=lambda: equalizer_manager.change_mixer('-', 8)),
        Text(master=equalizer_buttons_boxes[8], text=EQUAL_CONTROLS[8][4:], color="white", size=16, align="bottom")
    ], [
        PushButton(master=equalizer_buttons_boxes[9], image="icon/plus.png",
                   align="top", command=lambda: equalizer_manager.change_mixer('+', 9)),
        Text(master=equalizer_buttons_boxes[9], text="N/A", color="white", size=16),
        PushButton(master=equalizer_buttons_boxes[9], image="icon/minus.png",
                   align="bottom", command=lambda: equalizer_manager.change_mixer('-', 9)),
        Text(master=equalizer_buttons_boxes[9], text=EQUAL_CONTROLS[9][4:], color="white", size=16, align="bottom")
    ],
]

for i in range(10):
    equalizer_sliders[i].tk.config(relief=tk.FLAT, highlightthickness=0, showvalue=0)

equalizer_manager = EqualizerManager(equalizer_control_boxes, equalizer_sliders,
                                     equalizer_buttons_boxes, equalizer_buttons)

equalizer_low_close_box = Box(equalizer_low_window, layout="auto", align="bottom", width=480, height=80)

equalizer_low_close_btn = PushButton(equalizer_low_close_box, image="icon/back.png",
                                     command=window_manager.close_window, args=[EQUALIZER_LOW],
                                     align="left")
equalizer_low_text_btn = Text(equalizer_low_close_box, size=16, color="white", text="31 - 500 Hz", align="left")

equalizer_low_switch_btn = PushButton(equalizer_low_close_box, image="icon/back180.png",
                                      command=window_manager.open_window, args=[EQUALIZER_HIGH],
                                      align="right")

equalizer_low_load_btn = PushButton(equalizer_low_close_box, image="icon/restart.png",
                                    command=lambda: equalizer_manager.equalizer_load(0),
                                    align="right")
equalizer_low_save_btn = PushButton(equalizer_low_close_box, image="icon/save.png",
                                    command=equalizer_manager.equalizer_save,
                                    align="right")

equalizer_high_close_box = Box(equalizer_high_window, layout="auto", align="bottom", width=480, height=80)

equalizer_high_close_btn = PushButton(equalizer_high_close_box, image="icon/back.png",
                                      command=window_manager.close_window, args=[EQUALIZER_HIGH],
                                      align="left")
equalizer_high_text_btn = Text(equalizer_high_close_box, size=16, color="white", text="1 - 16 kHz", align="left")

equalizer_high_switch_btn = PushButton(equalizer_high_close_box, image="icon/back.png",
                                       command=window_manager.open_window, args=[EQUALIZER_LOW],
                                       align="right")
equalizer_high_load_btn = PushButton(equalizer_high_close_box, image="icon/restart.png",
                                     command=lambda: equalizer_manager.equalizer_load(1),
                                     align="right")
equalizer_high_save_btn = PushButton(equalizer_high_close_box, image="icon/save.png",
                                     command=equalizer_manager.equalizer_save,
                                     align="right")

########################################


########################################
# Pocasie ------------------------------
########################################

pocasie_window = Window(menu_window, bg="black", title=WEATHER, visible=False)
pocasie_window.hide()

window_manager.add_window(pocasie_window)

pocasie_box = Box(pocasie_window, layout="grid", align="top", width=480, height=240)

pocasie_teplota_txt = Text(pocasie_box, text="Teplota ", color="white", size=16, grid=[0, 2], align="left")
pocasie_vietor_txt = Text(pocasie_box, text="Rýchlosť vetra ", color="white", size=16, grid=[0, 3], align="left")
pocasie_oblaky_txt = Text(pocasie_box, text="Oblačnosť ", color="white", size=16, grid=[0, 4], align="left")
pocasie_tlak_txt = Text(pocasie_box, text="Tlak ", color="white", size=16, grid=[0, 5], align="left")
pocasie_vlhkost_txt = Text(pocasie_box, text="Vlhkosť ", color="white", size=16, grid=[0, 6], align="left")

pocasie_mesto_txt = Text(pocasie_box, text=" ", color="white", size=18, grid=[0, 0, 2, 1], align="top")
pocasie_stav_txt = Text(pocasie_box, text=" ", color="white", size=18, grid=[0, 1, 2, 1], align="top")
pocasie_teplota_data_txt = Text(pocasie_box, text="°C", color="white", size=18, grid=[1, 2], align="right")
pocasie_vietor_data_txt = Text(pocasie_box, text="m/s", color="white", size=18, grid=[1, 3], align="right")
pocasie_oblaky_data_txt = Text(pocasie_box, text="%", color="white", size=18, grid=[1, 4], align="right")
pocasie_tlak_data_txt = Text(pocasie_box, text="hPa", color="white", size=18, grid=[1, 5], align="right")
pocasie_vlhkost_data_txt = Text(pocasie_box, text="%", color="white", size=18, grid=[1, 6], align="right")

pocasie_icon_img = Picture(pocasie_box, image="icon/cloud.png", width=175, height=175, grid=[2, 0, 1, 7], align="right")

pocasie_buttons_box = Box(pocasie_window, layout="auto", align="bottom", width=480, height=80)
pocasie_buttons_box.hide()
close_pocasie_btn = PushButton(pocasie_buttons_box, image="icon/back.png",
                               command=window_manager.close_window, args=[WEATHER, 0],
                               align="left")

pocasie_date_txt = Text(pocasie_buttons_box, size=16, color="white", text="Date", align="left")

open_forecast_btn = PushButton(pocasie_buttons_box, image="icon/back180.png",
                               command=window_manager.open_window, args=[WEATHER, 1],
                               align="right")

fore_buttons_box = Box(pocasie_window, layout="auto", align="bottom", width=480, height=80)
# fore_buttons_box.hide()
close_fore_btn = PushButton(fore_buttons_box, image="icon/back.png",
                            command=window_manager.close_window, args=[WEATHER, 0],
                            align="left")
fore_date_txt = Text(fore_buttons_box, size=16, color="white", text="Date", align="left")

fore_next_btn = PushButton(fore_buttons_box, image="icon/back180.png",
                           command=lambda: weather_manager.switch_forecast(1),
                           align="right")
fore_prev_btn = PushButton(fore_buttons_box, image="icon/back.png",
                           command=lambda: weather_manager.switch_forecast(-1),
                           align="right")
########################################


########################################
# MP3 - radio - Bluetooth --------------
########################################

audio_menu_window = Window(menu_window, bg="black", title=AUDIO_MENU, visible=False)
audio_menu_window.hide()

window_manager.add_window(audio_menu_window)

top_music_box = Box(audio_menu_window, align="top", width=480, height=240)
bottom_music_box = Box(audio_menu_window, align="bottom", width=480, height=80)

radio_box = Box(top_music_box, align="top", width=480, height=80)
play_radio_btn = PushButton(radio_box, image="icon/radio.png",
                            command=player_manager.start_radio,
                            align="left")

bluetooth_box = Box(top_music_box, align="top", width=480, height=80)

bluetooth_btn = PushButton(bluetooth_box, image="icon/btoff.png",
                           command=player_manager.start_bluetooth,
                           align="left")

bluetooth_text_box = Box(bluetooth_box, align="left", width=398, height=82)

bluetooth_device_name_txt = Text(bluetooth_text_box, text=" ", align="left", size=20, color="white")
bluetooth_device_name_txt.tk.config(wraplength=398, justify=tk.LEFT, anchor="w", padx=30)

mp3_box = Box(top_music_box, align="top", width=480, height=80)

mp3_btn = PushButton(mp3_box, image="icon/usb_off.png",
                     command=player_manager.start_usb,
                     align="left")
usb_video_btn = PushButton(mp3_box, image="icon/folder_off.png",
                           command=window_manager.open_window,
                           args=[USB_VIDEO, True],
                           align="left")

random_play_btn = PushButton(bottom_music_box, image="icon/shuffleoff.png",
                             command=player_manager.shuffle,
                             align="right")

connect_to_bluetooth_btn = PushButton(bottom_music_box, image="icon/bton.png",
                                      command=window_manager.open_window,
                                      args=[BLUETOOTH_CONNECT],
                                      align="right")

close_music_menu_btn = PushButton(bottom_music_box, image="icon/back.png",
                                  command=window_manager.close_window, args=[AUDIO_MENU], align="left")

########################################

usb_video_view_window = Window(app, bg="black", title=USB_VIDEO, visible=False)
usb_video_view_window.hide()

window_manager.add_window(usb_video_view_window)

usb_video_listbox = ListBox(usb_video_view_window, height=240, width=480, items=[], scrollbar=True)

usb_video_listbox.text_color = "white"
usb_video_listbox.text_size = 20

usb_video_listbox.children[0].tk.config(relief=tk.FLAT, highlightthickness=0, width=10,
                                        activestyle=tk.NONE, selectmode=tk.SINGLE)
usb_video_listbox.children[1].tk.config(width=32, activebackground="#606060", bg="#808080", bd=0)

close_usb_video_view_box = Box(usb_video_view_window, align="bottom", width=480, height=80)

close_usb_video_view_btn = PushButton(close_usb_video_view_box, image="icon/back.png", align="left",
                                      command=window_manager.close_window, args=[USB_VIDEO])
usb_video_status_txt = Text(close_usb_video_view_box, text="", align="left", color="white")
play_usb_video_view_btn = PushButton(close_usb_video_view_box, image="icon/play.png", align="right",
                                     command=player_manager.play_usb_file_async)

########################################

bluetooth_connect_window = Window(menu_window, bg="black", title=BLUETOOTH_CONNECT, visible=False)
bluetooth_connect_window.hide()

window_manager.add_window(bluetooth_connect_window)

top_bt_box = Box(bluetooth_connect_window, align="top", width=480, height=240)

bt_devices_listbox = ListBox(top_bt_box, width=480, height=240, items=[], scrollbar=True,
                             command=player_manager.choose_bt_device)
bt_devices_listbox.text_color = "white"
bt_devices_listbox.text_size = 20
bt_devices_listbox.children[0].tk.config(relief=tk.FLAT, highlightthickness=0, width=10,
                                         activestyle=tk.NONE, selectmode=tk.SINGLE)
bt_devices_listbox.children[1].tk.config(width=32, activebackground="#606060", bg="#808080", bd=0)

bottom_bt_box = Box(bluetooth_connect_window, align="bottom", width=480, height=80)

bluetooth_connect_btn = PushButton(bottom_bt_box, image="icon/btoff.png",
                                   command=player_manager.scan_bt_async, align="right")

bluetooth_scan_close_btn = PushButton(bottom_bt_box, image="icon/back.png",
                                      command=window_manager.close_window, args=[BLUETOOTH_CONNECT], align="left")
bluetooth_scan_status_txt = Text(bottom_bt_box, text="", align="left", color="white")

########################################
# Control ------------------------------
########################################

control_box = Box(app, layout="auto", align="bottom", width=480, height=80)

prev_btn = PushButton(control_box, image="icon/previous.png",
                      command=player_manager.prev_track,
                      align="left")
play_stop_btn = PushButton(control_box, image="icon/stop.png",
                           command=player_manager.play_stop,
                           align="left")
next_btn = PushButton(control_box, image="icon/next.png",
                      command=player_manager.next_track,
                      align="left")
volume_down_btn = PushButton(control_box, image="icon/volume_down.png",
                             command=player_manager.volume_control, args=["-"], align="left")
volume_up_btn = PushButton(control_box, image="icon/volume_up.png",
                           command=player_manager.volume_control, args=["+"], align="left")
menu_btn = PushButton(control_box, image="icon/list.png",
                      command=window_manager.open_window, args=[MENU],
                      align="right")


next_btn.when_left_button_pressed = lambda: threading.Thread(target=player_manager.next_btn_pressed,
                                                             name="player_manager.next_btn_pressed").start()
next_btn.when_left_button_released = lambda: threading.Thread(target=player_manager.next_btn_released,
                                                              name="player_manager.next_btn_released").start()
next_btn.when_right_button_released = lambda: threading.Thread(target=player_manager.next_btn_released,
                                                               name="player_manager.next_btn_released").start()

prev_btn.when_left_button_pressed = lambda: threading.Thread(target=player_manager.prev_btn_pressed,
                                                             name="player_manager.prev_btn_pressed").start()
prev_btn.when_left_button_released = lambda: threading.Thread(target=player_manager.prev_btn_released,
                                                              name="player_manager.prev_btn_released").start()
prev_btn.when_right_button_released = lambda: threading.Thread(target=player_manager.prev_btn_released,
                                                               name="player_manager.prev_btn_released").start()

########################################


########################################
# Playlist -----------------------------
########################################

playlist_view_window = Window(app, bg="black", title=PLAYLIST_VIEW, visible=False)
playlist_view_window.hide()

window_manager.add_window(playlist_view_window)

playlist_view_listbox = ListBox(playlist_view_window, height=240, width=480, items=[], scrollbar=True,
                                command=player_manager.play_selected_track)

playlist_view_listbox.text_color = "white"
playlist_view_listbox.text_size = 20

playlist_view_listbox.children[0].tk.config(relief=tk.FLAT, highlightthickness=0, width=10,
                                            activestyle=tk.NONE, selectmode=tk.SINGLE)
playlist_view_listbox.children[1].tk.config(width=32, activebackground="#606060", bg="#808080", bd=0)

close_playlist_view_box = Box(playlist_view_window, align="bottom", width=480, height=80)

close_playlist_view_btn = PushButton(close_playlist_view_box, image="icon/back.png", align="left",
                                     command=window_manager.close_window, args=[PLAYLIST_VIEW])

playlist_remove_btn = PushButton(close_playlist_view_box, image="icon/clear.png", align="right", visible=False,
                                 command=player_manager.playlist_change_to_confirm_remove)

playlist_qrcode_btn = PushButton(close_playlist_view_box, image="icon/qr_code.png", align="right",
                                 command=player_manager.show_track_qrcode)
move_track_down_btn = PushButton(close_playlist_view_box, image="icon/down.png", align="right",
                                 command=player_manager.move_track_down, visible=False)
move_track_up_btn = PushButton(close_playlist_view_box, image="icon/up.png", align="right",
                               command=player_manager.move_track_up, visible=False)
playlist_open_browser_btn = PushButton(close_playlist_view_box, image="icon/internet_search.png", align="right",
                                       command=window_manager.open_window, args=[RADIO_BROWSER])
playlist_reload_btn = PushButton(close_playlist_view_box, image="icon/refresh.png", align="right",
                                 command=player_manager.reload_playlist, visible=False)
playlist_edit_mode_btn = PushButton(close_playlist_view_box, image="icon/list.png", align="right",
                                    command=window_manager.toggle_playlist_edit_mode)


########################################


########################################
# Radio browser ------------------------
########################################

radio_browser_window = Window(app, bg="black", title=RADIO_BROWSER, visible=False)
radio_browser_window.hide()

window_manager.add_window(radio_browser_window)

radio_browser_listbox = ListBox(radio_browser_window, height=240, width=480, items=[], scrollbar=True)

radio_browser_listbox.text_color = "white"
radio_browser_listbox.text_size = 20

radio_browser_listbox.children[0].tk.config(relief=tk.FLAT, highlightthickness=0, width=10,
                                            activestyle=tk.NONE, selectmode=tk.SINGLE)
radio_browser_listbox.children[1].tk.config(width=32, activebackground="#606060", bg="#808080", bd=0)

close_radio_browser_box = Box(radio_browser_window, align="bottom", width=480, height=80)

close_radio_browser_btn = PushButton(close_radio_browser_box, image="icon/back.png", align="left",
                                     command=window_manager.close_window, args=[RADIO_BROWSER])

radio_browser_play_btn = PushButton(close_radio_browser_box, image="icon/play.png", align="right",
                                    command=player_manager.radio_browser_play, visible=False)
radio_browser_next_btn = PushButton(close_radio_browser_box, image="icon/back180.png", align="right",
                                    command=player_manager.radio_browser_next)
radio_browser_prev_btn = PushButton(close_radio_browser_box, image="icon/back.png", align="right",
                                    command=player_manager.radio_browser_prev, visible=False)
radio_browser_add_btn = PushButton(close_radio_browser_box, image="icon/save.png", align="right",
                                   command=player_manager.radio_browser_add_to_playlist, visible=False)
radio_browser_sort_btn = PushButton(close_radio_browser_box, image="icon/sort.png", align="right",
                                    command=player_manager.radio_browser_change_sort, visible=False)

########################################


########################################
# QR code ------------------------------
########################################

qr_code_window = Window(app, bg="black", title=QR_CODE, visible=False)
qr_code_window.hide()

window_manager.add_window(qr_code_window)

qr_code_box = Box(qr_code_window, layout="auto", align="top", width=480, height=240)

qr_code_picture = Picture(qr_code_box, image="icon/nic.png", width=240, height=240, align="top")
qr_code_picture.when_clicked = lambda: window_manager.close_window(QR_CODE)

qr_code_bottom_box = Box(qr_code_window, layout="auto", align="bottom", width=480, height=80)

# qr_code_btn = PushButton(qr_code_bottom_box, image="icon/back.png",
#                          command=window_manager.close_window, args=[QR_CODE],
#                          align="left")
qr_code_text = Text(qr_code_bottom_box, size=16, color="white", align="bottom")
# qr_code_text.when_clicked = window_manager.open_browser
qr_code_text.tk.config(wraplength=480, justify=tk.CENTER, anchor=tk.NW)

########################################


########################################
# Image view ---------------------------
########################################

image_view_window = Window(app, bg="black", title=IMAGE_VIEW, visible=False)
image_view_window.hide()

window_manager.add_window(image_view_window)

image_view_picture = Picture(image_view_window, image="icon/nic.png", width=480, height=320, align="top")
image_view_picture.when_clicked = window_manager.close_image_view


########################################


########################################
# Info text ----------------------------
########################################

date_clock_top_padding_box = Box(app, align="top", width=480, height=10)

date_clock_box = Box(app, layout="auto", align="top", width=460, height=30)

date_btn = PushButton(date_clock_box, text="Date", align="left", command=window_manager.open_window,
                      args=[CALENDAR])
clock_btn = PushButton(date_clock_box, text="Time", align="right", command=window_manager.open_window,
                       args=[CLOCK])
temperature_btn = PushButton(date_clock_box, text="", align="top",
                             command=window_manager.open_window, args=[WEATHER, 0])

clock_btn.text_color = "white"
temperature_btn.text_color = "white"
date_btn.text_color = "white"
clock_btn.text_size = 20
temperature_btn.text_size = 18
date_btn.text_size = 20

info_box = Box(app, layout="grid", align="top", width=460, height=170)

info_spacing_txt = Text(info_box, text=" ", size=4, grid=[0, 0], align="left", color="white")
#info_spacing_left_txt = Box(info_box, width=10, grid=[0, 1, 1, 3], align="left")

info_lines_txt = [
    Text(info_box, text=" ", size=26, grid=[0, 1], align="left", color="#7CB342"),
    Text(info_box, text=" ", size=18, grid=[0, 3], align="left", color="white"),
    Text(info_box, text=" ", size=22, grid=[0, 4], align="left", color="white")
]

info_lines_txt[0].when_clicked = lambda: window_manager.open_window(PLAYLIST_VIEW)
info_lines_txt[1].tk.config(wraplength=460, justify=tk.LEFT, anchor=tk.NW)
info_lines_txt[2].tk.config(wraplength=460, justify=tk.LEFT, anchor=tk.NW)

volume_box = Box(app, layout="auto", align="bottom", width=460, height=30)
#volume_slider_padding = Text(volume_box, text=" ", size=10, align="left")
volume_slider = Slider(volume_box, align="left", horizontal=True, height=0, width=320, visible=False,
                       start=0, end=100, command=lambda x: player_manager.volume_control(value=x))
volume_slider.tk.config(relief=tk.FLAT, highlightthickness=0, showvalue=0)
volume_data_txt = Text(volume_box, text="N/A %", size=20, width=4, align="right", color="white")
volume_data_txt.tk.config(anchor="e")
volume_data_txt.when_clicked = window_manager.toggle_volume_slider
volume_icon_img = Picture(volume_box, image="icon/vol2.png", width=60, height=30, align="right")
position_txt = Text(volume_box, text="position", size=20, align="left", color="white")
player_manager.volume_reload()

########################################


play_icon = ImageTk.PhotoImage(file="icon/play.png")
stop_icon = ImageTk.PhotoImage(file="icon/stop.png")

threading.Thread(target=rss_manager.reload_links, name="rss_manager.reload_links", daemon=True).start()
threading.Thread(target=window_manager.reload_window, name="window_manager.reload_window", daemon=True).start()
threading.Thread(target=weather_manager.weather_reload_short, name="weather_manager.weather_reload_short", daemon=True).start()
threading.Thread(target=player_manager.info_reload, name="player_manager.info_reload", daemon=True).start()
threading.Thread(target=player_manager.reload_playlist, name="player_manager.reload_playlist", daemon=True).start()
threading.Thread(target=battery_manager.measure, name="battery_manager.measure", daemon=True).start()

webServer = HTTPServer(('', 8080), Server)
threading.Thread(target=webServer.serve_forever, name="webServer.serve_forever", daemon=True).start()
threading.Thread(target=mpc_idleloop, name="mpc_idleloop", daemon=True).start()
ws_manager.run()
threading.Thread(target=set_default_style, name="set_default_style", daemon=True).start()

loaded_img.image = "splash/2.png"

# Move to startup function, maybe
subprocess.run([MPC_PATH, "enable", "equal"])
subprocess.run([MPC_PATH, "disable", "bluealsa"])

if config.error_message != "":
    notice_window.show()

app.repeat(1000, window_manager.reload_window)

app.display()
