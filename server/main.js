const play_icon = "<svg style=\"width:24px;height:24px\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M8 6.82v10.36c0 .79.87 1.27 1.54.84l8.14-5.18c.62-.39.62-1.29 0-1.69L9.54 5.98C8.87 5.55 8 6.03 8 6.82z\" /></svg>";
const pause_icon = "<svg style=\"width:24px;height:24px\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M8 19c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2s-2 .9-2 2v10c0 1.1.9 2 2 2zm6-12v10c0 1.1.9 2 2 2s2-.9 2-2V7c0-1.1-.9-2-2-2s-2 .9-2 2z\" /></svg>";

// let top_bar = document.getElementById("top_bar");
let connection = document.getElementById("connection");
let loading = document.getElementById("loading");

let prev_btn = document.getElementById("prev_btn");
let playstop_btn = document.getElementById("playstop_btn");
let next_btn = document.getElementById("next_btn");
let voldown_btn = document.getElementById("voldown_btn");
let volup_btn = document.getElementById("volup_btn");
let volume = document.getElementById("volume");
let volume_slider = document.getElementById("volume_slider");

let show_more_btn = document.getElementById("show_more_btn");
let more_buttons = document.getElementById("more_buttons");
let youtube_url = document.getElementById("youtube_url");
let play_youtube_btn = document.getElementById("play_youtube_btn");
let youtube_search_results = document.getElementById("youtube_search_results");
let youtube_search_results_clear = document.getElementById("youtube_search_results_clear");

let reload_btn = document.getElementById("reload_btn");
let restart_btn = document.getElementById("restart_btn");
let send_ws_msg = document.getElementById("send_ws_msg");
let response_value = document.getElementById("response_value");
let open_ws = document.getElementById("open_ws");
let close_ws = document.getElementById("close_ws");

let show_debug = document.getElementById("show_debug");
let hidden_buttons = document.getElementById("hidden_buttons");
let config_save_btn = document.getElementById("config_save_btn");
let config_restart_btn = document.getElementById("config_restart_btn");

let equalizer_save = document.getElementById("equalizer_save");
let equalizer_load = document.getElementById("equalizer_load");

let config = document.getElementById("config");
let rss_add = document.getElementById("rss_add");
let stream_add = document.getElementById("stream_add");

let main_page = document.getElementById("main_page");
let equalizer_page = document.getElementById("equalizer_page");
let playlist_page = document.getElementById("playlist_page");
let playlist_page_header = document.querySelector("#playlist_page > h2:nth-child(1)");
let playlist_edit_mode = document.getElementById("playlist_edit_mode");
let playlist_add_station = document.getElementById("playlist_add_station");
let playlist_data = document.getElementById("playlist_data");
let rss_page = document.getElementById("rss_page");
let rss_page_header = document.querySelector("#rss_page > h2:nth-child(1)");
let rss_edit_mode = document.getElementById("rss_edit_mode");
let rss_add_link = document.getElementById("rss_add_link");
let rss_data = document.getElementById("rss_data");
let config_page = document.getElementById("config_page");

let pages = [main_page, equalizer_page, playlist_page, rss_page, config_page];

let main_btn = document.getElementById("main_btn");
let equalizer_btn = document.getElementById("equalizer_btn");
let playlist_btn = document.getElementById("playlist_btn");
let rss_btn = document.getElementById("rss_btn");
let config_btn = document.getElementById("config_btn");

let buttons = [main_btn, equalizer_btn, playlist_btn, rss_btn, config_btn];

let active_page;

let ws = null;

function wsStart() {

    ws = new WebSocket("ws://" + location.hostname + ":1337/");

    ws.onopen = function () {
        console.log("Connection is Established");
        connection.style.display = "none";
        ws.send("Message to Send");
    };

    ws.onclose = function () {
        connection.style.display = "inline";
        console.log("WebSocket closed");
        ws = null;
        restartWs();
    };

    ws.onmessage = function (evt) {
        let received_msg = evt.data;
        console.log("Received message: " + received_msg);
        if (received_msg === "reload_page") {
            window.location.reload(true);
            return;
        }
        if (received_msg !== "OK") {
            wsReloadState(received_msg);
        }
    };
}

wsStart();

function restartWs() {
    setTimeout(function () {
        if (ws === null) {
            config_btn.style.display = "none";
            wsStart();
        } else {
            restartWs();
        }
    }, 1000);
}

function wsReloadState(msg) {

    try {
        let msgJson = JSON.parse(msg);
        switch (msgJson.type) {
            case "data":
                parseData(msgJson);
                break;
        }
        return;
    } catch (e) {}

    let lines = msg.split(";");
    let parts, i;
    for (i = 0; i < lines.length; i++) {
        if (lines[i].startsWith("show_hide")) {
            setVisibility(lines[i].slice(10));
        } else {
            parts = lines[i].split("=");
            parts = [parts.shift(), parts.join('=')]
            if (parts[0] === "youtube_search_results") {
                setInnerHTML(parts[0], parts[1]);
                youtube_results_plays();
            } else if (parts[0].startsWith("eq_")) {
                setEqualizerValue(parts[0], parts[1]);
            } else {
                setElementValue(parts[0], parts[1]);
            }
        }
    }
}


function parseData(data) {
    switch (data.data) {
        case "player":
            parsePlayer(data.value);
            break;
        case "playlist":
            parsePlaylist(data.value);
            break;
        case "rss":
            parseRSS(data.value);
            break;
    }
}

function parsePlayer(playerData) {
    switch (playerData.type) {
        case "status":
            setPlayStopValue(playerData.data);
            break;
        case "volume":
            setVolumeValue(playerData.data);
            break;
        case "infolines":
            parseInfolines(playerData.data);
            break;
    }
}

function parseInfolines(lines) {
    lines.forEach(item => {
        setElementValue(Object.keys(item)[0], item[Object.keys(item)[0]]);
    });
}

function parsePlaylist(playlistData) {
    let playlistHTML = document.createElement("table");
    playlistHTML.className = "table playlist_table"

    playlistData.forEach(item => {
        let tr = document.createElement("tr");
        let track_play = document.createElement("td");
        let tpb = document.createElement("button");
        tpb.className = "track_play"
        tpb.innerText = item.name
        track_play.appendChild(tpb);

        let track_move_down = document.createElement("td");
        let tmd = document.createElement("button");
        tmd.className = "track_move_down"
        tmd.dataset.id = item.id;
        track_move_down.appendChild(tmd);

        let track_move_up = document.createElement("td");
        let tmu = document.createElement("button");
        tmu.className = "track_move_up"
        tmu.dataset.id = item.id;
        track_move_up.appendChild(tmu);

        let track_rename = document.createElement("td");
        let trb = document.createElement("button");
        trb.className = "track_rename"
        trb.dataset.name = item.name;
        track_rename.appendChild(trb);

        let track_show_url = document.createElement("td");
        let tsb = document.createElement("button");
        tsb.className = "track_show_url"
        let tsa = document.createElement("a");
        tsa.href = item.url;
        tsa.target = "_blank";
        tsb.appendChild(tsa);
        track_show_url.appendChild(tsb);

        tr.appendChild(track_play);
        tr.appendChild(track_move_down);
        tr.appendChild(track_move_up);
        tr.appendChild(track_rename);
        tr.appendChild(track_show_url);
        playlistHTML.appendChild(tr);
    })
    playlist_data.innerHTML = "";
    playlist_data.appendChild(playlistHTML);
    add_track_moves();
}


function parseRSS(RSSData) {
    let rssHTML = document.createElement("table");
    rssHTML.className = "table"

    RSSData.forEach(item => {
        let tr = document.createElement("tr");
        let rss_open = document.createElement("td");
        let tpb = document.createElement("button");
        tpb.className = "remove_rss_link"
        tpb.innerText = item.name
        tpb.dataset.url = item.url;
        rss_open.appendChild(tpb);

        let rss_move_down = document.createElement("td");
        let tmd = document.createElement("button");
        tmd.className = "rss_move_down"
        tmd.dataset.id = item.id;
        rss_move_down.appendChild(tmd);

        let rss_move_up = document.createElement("td");
        let tmu = document.createElement("button");
        tmu.className = "rss_move_up"
        tmu.dataset.id = item.id;
        rss_move_up.appendChild(tmu);

        tr.appendChild(rss_open);
        tr.appendChild(rss_move_down);
        tr.appendChild(rss_move_up);
        rssHTML.appendChild(tr);
    })
    rss_data.innerHTML = "";
    rss_data.appendChild(rssHTML);
    add_rss_moves();
    toggleLoading();
}


function sendWsCommand(command_type, command, value) {
    let message = {
        "type": "command",
        "command_type": command_type,
        "command": command,
        "value": value
    }
    console.log(JSON.stringify(message));
    ws.send(JSON.stringify(message));
}


function sendCommand(theReq) {
    const theUrl = "/" + theReq;
    const xhr = new XMLHttpRequest();
    xhr.open("GET", theUrl);
    xhr.send();
}

function setPlayStopValue(value) {
    if (value === "0") {
        playstop_btn.innerHTML = play_icon;
    } else if (value === "1") {
        playstop_btn.innerHTML = pause_icon;
    }
}

function setVolumeValue(value) {
    if (volume.innerText !== value) {
        volume.innerText = value;
    }
    volume_slider.value = value;
}

function setElementValue(element, value) {
    const selected = document.getElementById(element);
    selected.innerText = value;
}

function setInnerHTML(element, data) {
    const selected = document.getElementById(element);
    selected.innerHTML = data;
}

function toggleLoading(state) {
    if (state) {
        loading.style.display = "inline";
    } else {
        loading.style.display = "none";
    }
}

function setVisibility(data) {
    data = data.split(",");
    const selected = document.getElementById(data[0]);
    if (data[1] === "1") {
        selected.style.display = "inline";
    } else {
        selected.style.display = "none";
    }
}

// Equalizer
function equalizerControl(slider) {
    ws.send(slider.id + "=" + slider.value);
}

function setEqualizerValue(slider, value) {
    const slider_element = document.getElementById(slider);
    const slider_element_value = document.getElementById(slider + "_value");
    slider_element.value = value;
    slider_element_value.innerText = value + " %";
}

//Equalizer end

//Config

function saveConfig() {
    const str = config.value;
    const theUrl = "/?saveconfig";
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.responseText.toString() === "OK") {
                if (confirm("Config saved. Restart now?")) {
                    sendCommand('restart');
                }
            } else if (xhr.responseText.toString() === "ERROR") {
                alert("Error saving, config is locked");
            } else {
                alert("Error");
            }
        }
    }
    xhr.open("POST", theUrl);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("config=" + str + "\n");
}

//Config end


function change_page(page, button) {
    pages.forEach(function (item) {
        item.style.display = "none";
    });
    page.style.display = "block";

    buttons.forEach(function (item) {
        item.style.background = "#000";
    });
    button.style.background = "#333";
    active_page = page.id;
}

main_btn.addEventListener("click", function () {
    change_page(main_page, this);
})

equalizer_btn.addEventListener("click", function () {
    change_page(equalizer_page, this);
})

playlist_btn.addEventListener("click", function () {
    change_page(playlist_page, this);
})

playlist_page_header.addEventListener("click", function () {
    if (playlist_edit_mode.style.display === "none" || playlist_edit_mode.style.display === "") {
        playlist_edit_mode.style.display = "inline-block";
        playlist_add_station.style.display = "block";
    } else {
        playlist_edit_mode.style.display = "none";
        playlist_add_station.style.display = "none";
    }
    add_track_moves();
})

rss_btn.addEventListener("click", function () {
    change_page(rss_page, this);
})

rss_page_header.addEventListener("click", function () {
    if (rss_edit_mode.style.display === "none" || rss_edit_mode.style.display === "") {
        rss_edit_mode.style.display = "inline-block";
        rss_add_link.style.display = "block";
    } else {
        rss_edit_mode.style.display = "none";
        rss_add_link.style.display = "none";
    }
    add_rss_moves();
})

config_btn.addEventListener("click", function () {
    change_page(config_page, this);
})

prev_btn.addEventListener("click", function () {
    if (!mouseIsDown) {
        sendWsCommand("player", "track_prev");
    }
    mouseIsDown = false;
})
playstop_btn.addEventListener("click", function () {
    sendWsCommand("player", "track_playstop");
})
next_btn.addEventListener("click", function () {
    if (!mouseIsDown) {
        sendWsCommand("player", "track_next");
    }
    mouseIsDown = false;
})

let mouseTimer;
let mouseIsDown = false;

function mouseDown(f) {
    mouseTimer = window.setTimeout(function () {execMouseDown(f)},500);
}

function mouseUp() {
    if (mouseTimer) window.clearTimeout(mouseTimer);
}

function execMouseDown(f) {
    f();
    mouseIsDown = true;
    if (mouseIsDown) {
        mouseDown(f);
    }
}

["mousedown", "touchstart"].forEach(function (event) {
    prev_btn.addEventListener(event, function () {
        mouseDown(function () {sendWsCommand("player", "seek", "-5");})
    });
});

["mousedown", "touchstart"].forEach(function (event) {
    next_btn.addEventListener(event, function () {
        mouseDown(function () {sendWsCommand("player", "seek", "+5");})
    });
});

document.body.addEventListener("mouseup", mouseUp);
document.body.addEventListener("touchend", mouseUp);

voldown_btn.addEventListener("click", function () {
    sendWsCommand("player", "volumedown");
})
volup_btn.addEventListener("click", function () {
    sendWsCommand("player", "volumeup");
})

volume.addEventListener("click", function () {
    if (volume_slider.style.display === "none") {
        volume_slider.style.display = "inline";
    } else {
        volume_slider.style.display = "none";
    }
})
volume.addEventListener("wheel", function (e){
    sendWsCommand("player", 'volume' + (e.wheelDelta > 0 ? 'up' : 'down'));
});

volume_slider.addEventListener("change", function () {
    sendWsCommand("player", 'volume', volume_slider.value);
});

// volume_slider.addEventListener("wheel", function (e){
//     sendWsCommand('volume' + (e.wheelDelta > 0 ? 'up' : 'down'));
// });

show_more_btn.addEventListener("click", function () {
    if (more_buttons.style.display === "none" || more_buttons.style.display === "") {
        // this.style.animation = 'open 0.5s ease-in';
        // this.style.transform = 'rotate(180deg)';
        this.querySelector("#show_more_btn_icon").style.transform = 'rotateX(0deg)';
        more_buttons.style.display = "grid";
    } else {
        // this.style.animation = 'close 0.5s ease-in';
        // this.style.transform = 'rotate(360deg)';
        this.querySelector("#show_more_btn_icon").style.transform = 'rotateX(180deg)';
        more_buttons.style.display = "none";
    }
})

play_youtube_btn.addEventListener("click", function () {
    if (youtube_url.value == null || youtube_url.value === ""){
        let link = prompt("Enter youtube url", "");
        if (link != null && link !== "") {
            ws.send("youtube " + link);
        }
    } else {
        ws.send("youtube " + youtube_url.value);
    }

})

function youtube_results_plays() {
    let yt_results = document.querySelectorAll('.yt_result');

    yt_results.forEach(function (btn) {
            btn.addEventListener('click', function (){
                play_youtube_search(this.getAttribute('data-url'))
            });
        });
}
youtube_results_plays();

function play_youtube_search(url) {
    ws.send("youtube " + url);
}

youtube_search_results_clear.addEventListener("click", function () {
    youtube_search_results.innerHTML = "";
    youtube_url.value = "";
})

reload_btn.addEventListener("click", function () {
    sendCommand('reloadwebserver');
})
restart_btn.addEventListener("click", function () {
    sendCommand('restart');
})
send_ws_msg.addEventListener("click", function () {
    ws.send(response_value.value);
})
open_ws.addEventListener("click", function () {
    wsStart();
})
close_ws.addEventListener("click", function () {
    ws.close();
})

const eq_sliders = document.querySelectorAll('.slider');
eq_sliders.forEach(function (btn) {
    btn.addEventListener('change', function () {
        equalizerControl(this);
    });
});

equalizer_save.addEventListener("click", function () {
    ws.send("equalizersave");
})
equalizer_load.addEventListener("click", function () {
    ws.send("equalizerload");
})

connection.addEventListener("click", function () {
    window.location.reload(true);
})

show_debug.addEventListener('change', (event) => {
    if (event.currentTarget.checked) {
        hidden_buttons.style.display = "block";
    } else {
        hidden_buttons.style.display = "none";
    }
})

rss_add.addEventListener("click", function () {
    let url = document.getElementById("rss_url").value;
    if (url != null && url !== "" && url.indexOf("http") === 0) {
        sendWsCommand("rss", "rss_add", {"url": url});
        toggleLoading(true);
    } else if (url != null && url !== "") {
        alert("Invalid url");
    }
})

function rss_remove() {
    if (confirm("Do you want to remove \"" + this.textContent + "\"?")) {
        sendWsCommand("rss", "rss_remove", {"name": this.textContent});
        toggleLoading(true);
    }
}

function rss_move_up() {
    sendWsCommand("rss", "rss_move", {"id": this.dataset.id, "direction": "UP"});
}

function rss_move_down() {
    sendWsCommand("rss", "rss_move", {"id": this.dataset.id, "direction": "DOWN"});
}

function rss_open() {
    window.open(this.dataset.url, '_blank').focus();
}

function add_rss_moves() {
    let remove_rss_links = document.querySelectorAll('.remove_rss_link');
    let rss_move_up_el = document.querySelectorAll('.rss_move_up');
    let rss_move_down_el = document.querySelectorAll('.rss_move_down');

    if (rss_edit_mode.style.display === "none" || rss_edit_mode.style.display === "") {
        remove_rss_links.forEach(function (btn) {
            btn.removeEventListener('click', rss_remove);
            btn.addEventListener('click', rss_open);
        });

        rss_move_up_el.forEach(function (btn) {
            btn.removeEventListener('click', rss_move_up);
            btn.style.display = "none";
        });

        rss_move_down_el.forEach(function (btn) {
            btn.removeEventListener('click', rss_move_down);
            btn.style.display = "none";
        });
    } else {
        remove_rss_links.forEach(function (btn) {
            btn.removeEventListener('click', rss_open);
            btn.addEventListener('click', rss_remove);
        });

        rss_move_up_el.forEach(function (btn) {
            btn.addEventListener('click', rss_move_up);
            btn.style.display = "block";
        });

        rss_move_down_el.forEach(function (btn) {
            btn.addEventListener('click', rss_move_down);
            btn.style.display = "block";
        });
    }

}

stream_add.addEventListener("click", function () {
    let name = document.getElementById("stream_name").value;
    let url = document.getElementById("stream_url").value;
    if (url != null && url !== "" && url.indexOf("http") === 0 && name != null && name !== "") {
        sendWsCommand("playlist", "track_add", {"name": name, "url": url});
    } else if (url != null && url !== "") {
        alert("Invalid url");
    }
})

function play_track() {
    sendWsCommand("playlist", "track_play", this.textContent);
}

function rename_track() {
    let name = prompt("Enter new name", this.dataset.name);
    if (name != null && name !== "null" && name !== "" && name !== this.dataset.name) {
        sendWsCommand("playlist", "track_rename", {"old": this.dataset.name, "new": name});
    }
}

function playlist_move_up() {
    sendWsCommand("playlist", "track_move", {"id": this.dataset.id, "direction": "UP"});
}

function playlist_move_down() {
    sendWsCommand("playlist", "track_move", {"id": this.dataset.id, "direction": "DOWN"});
}

function playlist_remove() {
    if (confirm("Do you want to remove \"" + this.textContent + "\"?")) {
        sendWsCommand("playlist", "track_remove", this.textContent);

    }
}

function add_track_moves() {
    let track_play_el = document.querySelectorAll('.track_play');
    let track_move_up_el = document.querySelectorAll('.track_move_up');
    let track_move_down_el = document.querySelectorAll('.track_move_down');
    let track_rename_el = document.querySelectorAll('.track_rename');
    let track_show_url_el = document.querySelectorAll('.track_show_url');

    if (playlist_edit_mode.style.display === "none" || playlist_edit_mode.style.display === "") {
        track_play_el.forEach(function (btn) {
            btn.removeEventListener('click', playlist_remove);
            btn.addEventListener('click', play_track);
        });

        track_move_up_el.forEach(function (btn) {
            btn.style.display = "none";
            btn.removeEventListener('click', playlist_move_up);
        });
        track_move_down_el.forEach(function (btn) {
            btn.style.display = "none";
            btn.removeEventListener('click', playlist_move_down);
        });
        track_rename_el.forEach(function (btn) {
            btn.style.display = "none";
            btn.removeEventListener('click', rename_track);
        });
        track_show_url_el.forEach(function (btn) {
            btn.style.display = "none";
    });

    } else {
        track_play_el.forEach(function (btn) {
            btn.removeEventListener('click', play_track);
            btn.addEventListener('click', playlist_remove)
        });

        track_move_up_el.forEach(function (btn) {
            btn.style.display = "block";
            btn.addEventListener('click', playlist_move_up);
        });
        track_move_down_el.forEach(function (btn) {
            btn.style.display = "block";
            btn.addEventListener('click', playlist_move_down);
        });
        track_rename_el.forEach(function (btn) {
            btn.style.display = "block";
            btn.addEventListener('click', rename_track);
        });
        track_show_url_el.forEach(function (btn) {
            btn.style.display = "block";
    });
    }
}

config_save_btn.addEventListener("click", function () {
    saveConfig();
})

config_restart_btn.addEventListener("click", function () {
    if (confirm("Do you want to restart without saving?")) {
        sendCommand('restart');
    }
})

window.onload = function() {
    let page = sessionStorage.getItem("page");

    if (page != null && page !== "config") {
        change_page(document.getElementById(page + "_page"), document.getElementById(page + "_btn"));
    } else {
        change_page(main_page, main_btn);
    }

    if (config.innerText === "") {
        config_btn.style.display = "none";
    }
}

window.onbeforeunload = function() {
        sessionStorage.setItem("page", active_page.slice(0, -5));
}